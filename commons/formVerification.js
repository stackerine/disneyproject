const validator = require('validator');

const isNotEmpty = field => !validator.isEmpty(field);

const isDifferentValue = (currentValue, newValue) => currentValue !== newValue;

const isValidPassword = password =>
  validator.isAscii(password) && password.length >= 6 && password.length <= 18;

const isValidUsername = username => validator.isAlphanumeric(username);

const isValidEmail = email => validator.isEmail(email);

const isValidPhoneNumber = phoneNumber =>
  validator.isMobilePhone(phoneNumber, 'fr-FR');

module.exports = {
  isNotEmpty,
  isValidPassword,
  isValidUsername,
  isValidEmail,
  isValidPhoneNumber,
  isDifferentValue,
};
