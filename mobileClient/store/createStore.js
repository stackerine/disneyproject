import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import RNFirebase from 'react-native-firebase';
import rootReducer from '../reducers/rootReducer';

const reactNativeFirebaseConfig = {
  debug: true,
};
// for more config options, visit http://docs.react-redux-firebase.com/history/v2.0.0/docs/api/compose.html
const reduxFirebaseConfig = {
  userProfile: 'users', // save users profiles to 'users' collection
  enableRedirectHandling: false,
};

export default (initialState = { firebase: {} }) => {
  // initialize firebase
  const firebase = RNFirebase.initializeApp(reactNativeFirebaseConfig);

  const middleware = [
    // make getFirebase available in third argument of thunks
    thunk,
  ];

  const store = createStore(
    rootReducer,
    initialState, // initial state
    // window.__REDUX_DEVTOOLS_EXTENSION__ &&
    //   window.__REDUX_DEVTOOLS_EXTENSION__(),
    composeWithDevTools(applyMiddleware(...middleware))
  );
  return store;
};
