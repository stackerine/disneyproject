import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

const ComicButton = ({ text }) => {
  const offset = 10;
  return (
    <View
      style={{
        backgroundColor: 'black',
        marginTop: offset,
      }}
    >
      <TouchableOpacity
        style={{
          backgroundColor: 'white',
          top: -offset,
          left: -offset,
          paddingHorizontal: 20,
          paddingVertical: 10,
          borderColor: 'black',
          borderWidth: 3,
        }}
      >
        <Text
          style={{
            fontSize: 20,
          }}
        >
          {text}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default ComicButton;
