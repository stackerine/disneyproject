import React from 'react';
import { View, Dimensions } from 'react-native';

const ComicCard = ({ children }) => {
  const { width } = Dimensions.get('window');
  const offset = 10;
  return (
    <View style={{ width: width - 2 * offset, left: offset * 2 }}>
      <View
        style={{
          backgroundColor: 'black',
          marginTop: offset,
        }}
      >
        <View
          style={{
            backgroundColor: 'white',
            top: -offset,
            left: -offset,
            paddingHorizontal: 20,
            paddingVertical: 10,
            borderColor: 'black',
            borderWidth: 3,
          }}
        >
          {children}
        </View>
      </View>
    </View>
  );
};

export default ComicCard;
