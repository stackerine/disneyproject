import React, { Component } from 'react';
import { Animated, Easing } from 'react-native';
import {
  Text as NBText,
  Icon as NBIcon,
  Button as NBButton,
  Spinner,
} from 'native-base';

export default class ButtonActionSpinner extends Component {
  constructor(props) {
    super(props);
    this.loaderAnim = new Animated.Value(0.01);
  }

  showAnim = showLoader => {
    Animated.timing(this.loaderAnim, {
      useNativeDriver: true,
      toValue: showLoader ? 1 : 0.01,
      duration: 150,
      easing: Easing.ease,
    }).start();
  };

  makeAction = async () => {
    this.showAnim(true);
    await this.props.action();
    setTimeout(() => {
      this.showAnim(false);
    }, 150);
  };

  render() {
    const {
      action,
      buttonStyle = {},
      text,
      arrowColor = 'white',
      spinnerColor = 'white',
    } = this.props;

    const loaderValue = this.loaderAnim.interpolate({
      inputRange: [0, 1],
      outputRange: [-40, 10],
    });

    return (
      <NBButton
        onPress={this.makeAction}
        rounded
        primary
        large
        style={{ ...buttonStyle, overflow: 'hidden' }}
      >
        <NBText>{text}</NBText>
        <Animated.View
          style={{
            overflow: 'hidden',
            transform: [{ translateY: loaderValue }],
          }}
        >
          <Spinner color={spinnerColor} />
          <NBIcon
            name="arrow-forward"
            fontSize="37"
            style={{ color: arrowColor }}
          />
        </Animated.View>
      </NBButton>
    );
  }
}
