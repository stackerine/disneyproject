import React, { Component } from 'react';
import getThemeApplication from '../services/firebase';

withTheme = WrappedComponent => {
  return class extends Component {
    state = {
      theme: '',
    };

    componentDidMount = () => {
      this.fetchApplicationTheme();
    };

    fetchApplicationTheme = async () => {
      this.setState({ theme: await getThemeApplication() });
    };

    render() {
      return <WrappedComponent {...this.props} theme={this.state.theme} />;
    }
  };
};

export default withTheme;
