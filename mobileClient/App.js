/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import RNFirebase from 'react-native-firebase';
import { Provider } from 'react-redux';
import { ReactReduxFirebaseProvider } from 'react-redux-firebase';
import { createFirestoreInstance } from 'redux-firestore';
import { Root } from 'native-base';
import {
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer,
} from 'react-navigation';

import createStore from './store/createStore';

import LoginPage from './components/login-page/LoginPage';
import AppScreen from './components/app-screen-page/redux-connected/AppScreen';
import AccountPage from './components/account-page/redux-connected/AccountPage';
import InfoElem from './components/account-page/redux-connected/InfoElem';
import EditApp from './components/editApp/EditApp';
import WaitEmailValidationPage from './components/login-page/redux-connected/WaitEmailValidation';
import SplashScreen from './components/login-page/redux-connected/SplashScreen';
import CreateAppPage from './components/application-page/CreateAppPage';

// Store Initialization
const initialState = { firebase: {} };
const store = createStore(initialState);

const AppStack = createStackNavigator(
  {
    Home: AppScreen,
    CreateApp: {
      screen: CreateAppPage,
      navigationOptions: () => ({
        title: 'Create an application',
      }),
    },
    EditApp: {
      screen: EditApp,
      navigationOptions: () => ({
        title: 'Edit application',
      }),
    },
  },

  {
    initialRouteName: 'Home',
  }
);

const AccountStack = createStackNavigator({
  Home: {
    screen: AccountPage,
    navigationOptions: () => ({
      title: `My account`,
      headerBackTitle: 'Back',
    }),
  },
  InfoElem: {
    screen: InfoElem,
    navigationOptions: ({ navigation }) => ({
      title: `${navigation.state.params.title}`,
    }),
  },
});

const Navigation = createAppContainer(
  createSwitchNavigator(
    {
      SplashScreen: SplashScreen,
      AuthScreen: LoginPage,
      WaitEmailValidation: WaitEmailValidationPage,
      App: AppStack,
      AccountPage: AccountStack,
    },
    {
      initialRouteName: 'SplashScreen',
    }
  )
);

const rrfConfig = {
  userProfile: 'users',
  enableRedirectHandling: false,
};

const rrfProps = {
  firebase: RNFirebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance,
};

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <ReactReduxFirebaseProvider {...rrfProps}>
          <Root>
            <Navigation />
          </Root>
        </ReactReduxFirebaseProvider>
      </Provider>
    );
  }
}
