/**
 * Test if the url contain verification email mode
 * @param {String} url
 */
export const isVerificationEmailUrl = url => {
  const verificationUrlPattern = new RegExp('mode=verifyEmail', 'gi');

  return verificationUrlPattern.test(url);
};

export const getVerificationEmailOobCode = url => {
  const matchedGroups = url.match(/oobCode=([\w-]*)/i);
  if (matchedGroups.length === 2) {
    return matchedGroups[1];
  }
  return null;
};
