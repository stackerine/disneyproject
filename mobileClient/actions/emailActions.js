import RNFirebase from 'react-native-firebase';
import {
  EMAIL_VERIFICATION_REQUEST,
  EMAIL_VERIFICATION_SUCCESS,
  EMAIL_VERIFICATION_FAILURE,
  SEND_EMAIL_VERIFICATION_REQUEST,
  SEND_EMAIL_VERIFICATION_SUCCESS,
  SEND_EMAIL_VERIFICATION_FAILURE,
  SET_INITIAL_STATE,
} from '../actionTypes';

/**
|--------------------------------------------------
| Actions
|--------------------------------------------------
*/
/**
 * Send oobCode to verify user email throughout Firebase
 * @param {String} oobCode code to validate user's email
 */
export const emailVerificationWithOobCode = oobCode => async dispatch => {
  dispatch({ type: EMAIL_VERIFICATION_REQUEST });

  try {
    await RNFirebase.auth().applyActionCode(oobCode);
    dispatch({ type: EMAIL_VERIFICATION_SUCCESS });
  } catch (error) {
    console.log(error);
    dispatch({
      type: EMAIL_VERIFICATION_FAILURE,
      payload: emailFailMessage(error.code),
    });
  }
};

/**
 * Send email to verify user email
 */
export const resendVerificationEmail = () => async dispatch => {
  dispatch({ type: SEND_EMAIL_VERIFICATION_REQUEST });

  try {
    await RNFirebase.auth().currentUser.sendEmailVerification();
    dispatch({ type: SEND_EMAIL_VERIFICATION_SUCCESS });
  } catch (error) {
    console.log(error);
    dispatch({
      type: SEND_EMAIL_VERIFICATION_FAILURE,
      payload: emailFailMessage(error.code),
    });
  }
};

export const clearState = () => ({ type: SET_INITIAL_STATE });

// Email errors
const emailFailMessage = errorCode => {
  switch (errorCode) {
    case 'auth/expired-action-code':
      return 'Email code verification has expired';
    case 'auth/invalid-action-code':
      return 'Email code verification is invalid.';
    case 'auth/user-disabled':
      return 'User was disabled';
    case 'auth/user-not-found':
      return 'The user was deleted';
    default:
      return 'Email verification failed.';
  }
};
