import RNFirebase from 'react-native-firebase';
import {
  SIGN_IN_REQUEST,
  SIGN_IN_SUCCESS,
  SIGN_IN_FAILURE,
  SIGN_UP_REQUEST,
  SIGN_UP_SUCCESS,
  SIGN_UP_FAILURE,
  USER_PROFIL_UPDATE_REQUEST,
  USER_PROFIL_UPDATE_SUCCESS,
  USER_PROFIL_UPDATE_FAILURE,
  SET_INITIAL_STATE,
} from '../actionTypes';

/**
|--------------------------------------------------
| Actions
|--------------------------------------------------
*/
/**
 * Authenticate user throughout Firebase Auth system
 * @param {Object} with email and password for authentication
 */
export const signInUser = ({ email, password }) => async dispatch => {
  dispatch({ type: SIGN_IN_REQUEST });

  try {
    const user = await RNFirebase.auth().signInWithEmailAndPassword(
      email,
      password
    );
    dispatch({ type: SIGN_IN_SUCCESS, payload: user });
  } catch (error) {
    dispatch({
      type: SIGN_IN_FAILURE,
      payload: authFailMessage(error.code),
    });
  }
};

/**
 * Create user throughout Firebase Auth system
 * @param {Object} with email and password to create user
 */
export const signUpUser = ({ email, password }) => async (
  dispatch,
  getState
) => {
  dispatch({ type: SIGN_UP_REQUEST });

  try {
    const firestore = RNFirebase.firestore();
    const newUser = await RNFirebase.auth().createUserWithEmailAndPassword(
      email,
      password
    );
    const profileSnapshot = await firestore
      .collection('users')
      .doc(newUser.user.uid)
      .get();

    await profileSnapshot.ref.set({}, { merge: true });

    await newUser.user.sendEmailVerification();
    dispatch({ type: SIGN_UP_SUCCESS, payload: newUser });
  } catch (error) {
    console.error(error);
    dispatch({ type: SIGN_UP_FAILURE, payload: authFailMessage(error.code) });
  }
};

/**
 * Update user profile on Firebase
 * @param {Object} with new profile information
 */
export const updateUserProfile = (
  { field, value },
  firebase
) => async dispatch => {
  console.log('field : ' + field, 'value : ' + value, 'firebase : ' + firebase);
  try {
    dispatch({ type: USER_PROFIL_UPDATE_REQUEST });

    // TODO Change Email / Password / Phone number are security sensitive operation that need more work

    await RNFirebase.auth().currentUser.updateProfile({ [field]: value });
    firebase.reloadAuth();
    dispatch({ type: USER_PROFIL_UPDATE_SUCCESS });
  } catch (error) {
    console.log(error);
    dispatch({
      type: USER_PROFIL_UPDATE_FAILURE,
      payload: 'Error happened. Please try again later',
    });
  }
};

/**
 * Signout user throughout Firebase Auth system
 */
export const signOutUser = () => dispatch => {
  dispatch({ type: SET_INITIAL_STATE });

  RNFirebase.auth().signOut();
};

// Firebase Auth error
const authFailMessage = errorCode => {
  switch (errorCode) {
    case 'auth/invalid-email':
      return 'Email is invalid.';
    case 'auth/user-disabled':
      return 'User is disabled.';
    case 'auth/user-not-found':
      return 'User not found.';
    case 'auth/wrong-password':
      return 'Password is invalid.';
    case 'auth/email-already-in-use':
      return 'Email address is already in use.';
    case 'auth/weak-password':
      return 'Password is not strong enough.';
    default:
      return 'Authentication failed.';
  }
};
