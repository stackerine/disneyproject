/**
 |--------------------------------------------------
 | Types
 |--------------------------------------------------
 */
export const SIGN_UP_REQUEST = 'SIGN_UP_REQUEST';
export const SIGN_UP_SUCCESS = 'SIGN_UP_SUCCESS';
export const SIGN_UP_FAILURE = 'SIGN_UP_FAILURE';
export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST';
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS';
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE';
export const USER_PROFIL_UPDATE_REQUEST = 'USER_PROFIL_UPDATE_REQUEST';
export const USER_PROFIL_UPDATE_SUCCESS = 'USER_PROFIL_UPDATE_SUCCESS';
export const USER_PROFIL_UPDATE_FAILURE = 'USER_PROFIL_UPDATE_FAILURE';
export const SET_INITIAL_STATE = 'SET_INITIAL_STATE';
