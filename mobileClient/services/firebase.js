import firebase from 'react-native-firebase';
import { getUser } from './auth';

export const firestoreFailMessage = ({ code = '' } = { code: '' }) => {
  switch (code) {
    case 'firestore/cancelled':
      return 'Operation cancelled';
    case 'firestore/invalid-argument':
      return 'Invalid parameter.';
    case 'firestore/deadline-exceeded':
      return 'Operation timeout.';
    case 'firestore/not-found':
      return 'Document not found';
    case 'firestore/already-exists':
      return 'Document already exists';
    case 'firestore/permission-denied':
      return 'Permission denied';
    case 'firestore/resource-exhausted':
      return 'System out of space';
    case 'firestore/failed-precondition':
      return 'System is not ready for this operation';
    case 'firestore/aborted':
      return 'Operation aborted';
    case 'firestore/out-of-range':
      return 'Invalid parameter';
    case 'firestore/unimplemented':
      return 'Operation is not authorized';
    case 'firestore/internal':
      return 'Internal error';
    case 'firestore/unavailable':
      return 'Service is currently unavailable';
    case 'firestore/data-loss':
      return 'Unrecoverable data';
    case 'firestore/unauthenticated':
      return 'Operation forbidden';
    default:
      return 'Error from database Firestore';
  }
};

export const subscribeToUserApplication = async onApplicationChange => {
  const { uid } = await getUser();
  const db = firebase.firestore();
  const unsubscriber = db
    .collection('apps')
    .where('subscribers', 'array-contains', uid)
    .onSnapshot(onApplicationChange);

  return unsubscriber;
};

export const getThemeApplication = async () => {
  const db = firebase.firestore();
  const userApp = await db
    .collection('apps')
    .doc('99') // TODO : Fetch ID application
    .get();
  const { theme } = userApp.data();
  return theme;
};

export const changeThemeApplication = async theme => {
  const db = firebase.firestore();
  await db
    .collection('apps')
    .doc('99') // TODO : Fetch ID application
    .update({ theme });
};

export const saveChangeInfos = async ({ appId, data }) => {
  try {
    const db = firebase.firestore();
    await db
      .collection('apps')
      .doc(appId)
      .update({ ...data, lastUpdate: Date.now() });
  } catch (error) {
    console.log(error);
  }
};
