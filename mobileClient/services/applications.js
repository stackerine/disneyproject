import axios from 'axios';
import Config from 'react-native-config';
import { firestoreFailMessage } from './firebase';
import { getUser, getAuthToken } from './auth';

export const saveApp = async ({ id, title, description, isPublic }) => {
  try {
    const token = await getAuthToken();
    const { data } = await axios.post(
      `${Config.REACT_APP_HOST}/api/v1/applications`,
      { id, title, description, isPublic },
      { headers: { Authorization: `Bearer ${token}` } }
    );
    return data;
  } catch (e) {
    console.log('erreur:', e);
  }
};

export const createApplication = async (application, firestore) => {
  try {
    const { title, description, isPublic } = application;
    const { uid: userId } = await getUser();

    const collectionRef = firestore.collection('apps');

    const {
      _documentPath: { _parts },
    } = await collectionRef.add({
      title,
      description,
      icon: '',
      public: isPublic,
      subscribers: [userId.toString()],
      maintainers: [userId.toString()],
      theme: 'material',
      orderModules: [],
      lastUpdate: Date.now(),
    });
    return { error: null, application: _parts[1] };
  } catch (error) {
    console.log(error);
    return { error: firestoreFailMessage(error), application: null };
  }
};

