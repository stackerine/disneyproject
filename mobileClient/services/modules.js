import { firestoreFailMessage } from './firebase';

export const saveChangeModules = async ({ appId, modules }, firestore) => {
  try {
    const result = await Promise.all(
      modules.map(({ id, ...moduleInfos }) => {
        if (id) {
          // TODO SAVE MODULE CHANGE
          return { id };
        } else {
          const newModule = firestore
            .collection('apps')
            .doc(appId)
            .collection('modules')
            .add(moduleInfos);

          return newModule;
        }
      })
    );

    const newOrder = result.map(({ id }) => id);

    await firestore
      .collection('apps')
      .doc(appId)
      .update({ orderModules: newOrder });

    return { error: null };
  } catch (e) {
    console.log(e);
    return { error: firestoreFailMessage(e) };
  }
};
