import RNFirebase from 'react-native-firebase';

export const getUser = () => RNFirebase.auth().currentUser;

export const isVerifiedUser = () => {
  return (
    RNFirebase.auth().currentUser && RNFirebase.auth().currentUser.emailVerified
  );
};
