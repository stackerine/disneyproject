import axios from 'axios';
import Config from 'react-native-config';
import md5 from 'md5';
import { getAuthToken } from './auth';
import {
  isValidUsername,
  isValidPhoneNumber,
  isNotEmpty,
} from '../../commons/formVerification';

export const createUser = async (email, password) => {
  const { status, data } = await axios.post(
    `${Config.REACT_APP_HOST}/api/v1/users`,
    {
      email,
      password: md5(password),
    }
  );
  return { status, data };
};

export const resendConfirmationTokenForEmail = async email => {
  try {
    const {
      data: { message },
    } = await axios.post(`${Config.REACT_APP_HOST}/api/v1/email-confirmation`, {
      email,
    });

    return { status: 'success', message };
  } catch (e) {
    const {
      response: {
        data: { message = 'An error occured. Please try again later...' },
      } = { data: { message } },
    } = e;

    return { status: 'error', message };
  }
};

export const getUser = async () => {
  try {
    const { data: userInfo } = await axios.get(
      `${Config.REACT_APP_HOST}/api/v1/users/me`,
      {
        headers: { Authorization: `Bearer ${await getAuthToken()}` },
      }
    );
    return userInfo;
  } catch (e) {
    console.log(e);
  }
};

// TODO : PUT USER INTO FIREBASE
export const updateUserInfo = async (dataToUpdate, value) => {
  const isUsernameData = dataToUpdate === 'username' && isValidUsername(value);
  const isPhoneNumberData =
    dataToUpdate === 'phone_number' && isValidPhoneNumber(value);
  const isDataToDelete = !isNotEmpty(value);

  if (isUsernameData || isDataToDelete || isPhoneNumberData) {
    try {
      const { data: userInfo } = await axios.put(
        `${Config.REACT_APP_HOST}/api/v1/user`,
        { dataToUpdate, value },
        { headers: { Authorization: `Bearer ${await getAuthToken()}` } }
      );
      return userInfo;
    } catch (e) {
      console.log(e);
    }
  } else {
    return {
      isError: true,
      message: `Please, enter a valid ${dataToUpdate}.`,
    };
  }
};
