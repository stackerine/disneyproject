import React from 'react';
import { Button, Text, StyleProvider } from 'native-base';
import getTheme from '../../native-base-theme/components';
import withTheme from '../../hoc/withTheme';
import { toggleTheme } from './index';

const ButtonThemed = ({ children, theme }) => {
  return (
    <StyleProvider style={getTheme(toggleTheme(theme))}>
      <Button>
        <Text>{children}</Text>
      </Button>
    </StyleProvider>
  );
};

export default withTheme(ButtonThemed);
