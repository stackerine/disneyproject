import commonColor from '../../native-base-theme/variables/commonColor';
import material from '../../native-base-theme/variables/material';
import platform from '../../native-base-theme/variables/platform';

import ButtonThemed from './ButtonThemed';
import TextThemed from './TextThemed';

// TODO : déplacer la fonction dans les paramètres de l'application
const toggleTheme = theme => {
  theme === 'commonColor' && commonColor;
  theme === 'material' && material;
  theme === 'platform' && platform;
};

export { toggleTheme, ButtonThemed, TextThemed };
