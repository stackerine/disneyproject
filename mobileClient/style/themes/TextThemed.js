import React from 'react';
import { Text, StyleProvider } from 'native-base';
import getTheme from '../../native-base-theme/components';
import withTheme from '../../hoc/withTheme';
import { toggleTheme } from './index';

const TextThemed = ({ children, theme }) => {
  return (
    <StyleProvider style={getTheme(toggleTheme(theme))}>
      <Text>{children}</Text>
    </StyleProvider>
  );
};

export default withTheme(TextThemed);
