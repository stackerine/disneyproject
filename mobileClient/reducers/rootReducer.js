import { combineReducers } from 'redux';
import { firestoreReducer } from 'redux-firestore';
import { firebaseReducer } from 'react-redux-firebase';
import authReducer from './auth';
import emailActionsReducer from './emailActions';
import applicationCreation from './applicationCreation';

const rootReducer = combineReducers({
  auth: authReducer,
  emailActions: emailActionsReducer,
  firestore: firestoreReducer,
  firebase: firebaseReducer,
  applicationCreation,
});

export default rootReducer;
