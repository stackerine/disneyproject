import {
  EMAIL_VERIFICATION_REQUEST,
  EMAIL_VERIFICATION_SUCCESS,
  EMAIL_VERIFICATION_FAILURE,
  SEND_EMAIL_VERIFICATION_REQUEST,
  SEND_EMAIL_VERIFICATION_SUCCESS,
  SEND_EMAIL_VERIFICATION_FAILURE,
  SET_INITIAL_STATE,
} from '../actionTypes';

/**
 |--------------------------------------------------
 | Reducer
 |--------------------------------------------------
 */
const REQUEST_INITIAL_STATE = {
  error: '',
  loading: false,
  status: null,
};

const INITIAL_STATE = {
  sendEmailVerification: REQUEST_INITIAL_STATE,
  emailVerification: REQUEST_INITIAL_STATE,
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SEND_EMAIL_VERIFICATION_REQUEST:
      return {
        ...state,
        ...INITIAL_STATE,
        sendEmailVerification: { ...REQUEST_INITIAL_STATE, loading: true },
      };
    case EMAIL_VERIFICATION_REQUEST:
      return {
        ...state,
        ...INITIAL_STATE,
        emailVerification: { ...REQUEST_INITIAL_STATE, loading: true },
      };
    case SEND_EMAIL_VERIFICATION_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        sendEmailVerification: {
          ...REQUEST_INITIAL_STATE,
          status: 'Check your email box to verify your account.',
        },
      };
    case EMAIL_VERIFICATION_SUCCESS:
      return {
        ...state,
        ...INITIAL_STATE,
        emailVerification: {
          ...REQUEST_INITIAL_STATE,
          status: 'Your account is now activated',
        },
      };
    case SEND_EMAIL_VERIFICATION_FAILURE:
      return {
        ...state,
        ...INITIAL_STATE,
        sendEmailVerification: {
          ...REQUEST_INITIAL_STATE,
          error: action.payload,
        },
      };
    case EMAIL_VERIFICATION_FAILURE:
      return {
        ...state,
        ...INITIAL_STATE,
        emailVerification: { ...REQUEST_INITIAL_STATE, error: action.payload },
      };
    case SET_INITIAL_STATE:
      return { ...state, ...INITIAL_STATE };
    default:
      return state;
  }
};

export default reducer;
