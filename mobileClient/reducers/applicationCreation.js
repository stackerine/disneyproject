import {
  APPLICATION_CREATION_REQUEST,
  APPLICATION_CREATION_FAILURE,
  APPLICATION_CREATION_SUCCESS,
} from '../actionTypes';

const INITIAL_STATE = {
  loading: false,
  error: null,
  application: null,
};

const reducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case APPLICATION_CREATION_REQUEST:
      return { ...state, ...INITIAL_STATE, loading: true };
    case APPLICATION_CREATION_SUCCESS:
      return { ...state, ...INITIAL_STATE, application: action.payload };
    case APPLICATION_CREATION_FAILURE:
      return { ...state, ...INITIAL_STATE, error: action.payload };
    default:
      return state;
  }
};

export default reducer;
