import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Text as NBText,
  Button as NBButton,
  Icon as NBIcon,
  H1,
  Toast,
} from 'native-base';
import ButtonActionSpinner from '../../library/molecules/ButtonActionSpinner';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
    this.passwordRef = null;
    this.actionButtonRef = null;
  }

  componentDidUpdate({ authError: prevAuthError, user: prevUser }) {
    const { authError, user, isActive } = this.props;

    if (isActive) {
      if (authError && prevAuthError !== authError) {
        Toast.show({
          text: authError,
          type: 'danger',
        });
      } else if (user && prevUser !== user) {
        this.props.loginSuccess();
      }
    }
  }

  updateLogin = (field, text) => {
    this.setState({ [field]: text });
  };

  focusOnPassword = () => {
    this.passwordRef._root.focus();
  };

  onSubmit = () => {
    this.actionButtonRef.makeAction();
  };

  login = async () => {
    const { signInUser, auth } = this.props;
    const { email, password } = this.state;

    signInUser({ email: email.toLowerCase(), password });
  };

  render() {
    const { height, width, loading } = this.props;
    const { email, password } = this.state;
    const containerLayout = {
      position: 'absolute',
      height: height,
      width: width,
    };

    return (
      <Container style={containerLayout}>
        <Content style={{ ...containerLayout, zIndex: 1 }}>
          <H1 style={{ textAlign: 'center' }}>Login</H1>
          <Form>
            <Item last floatingLabel>
              <Label>Email</Label>
              <Input
                textContentType="username"
                value={email}
                onChangeText={this.updateLogin.bind(null, 'email')}
                returnKeyType="next"
                onSubmitEditing={this.focusOnPassword}
              />
            </Item>
            <Item last floatingLabel>
              <Label>Password</Label>
              <Input
                getRef={ref => (this.passwordRef = ref)}
                secureTextEntry={true}
                textContentType="password"
                value={password}
                onChangeText={this.updateLogin.bind(null, 'password')}
                returnKeyType="next"
                onSubmitEditing={this.onSubmit}
              />
            </Item>
          </Form>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
            }}
          >
            <TouchableOpacity style={{ marginTop: 10 }}>
              <Text>Forgot Password</Text>
            </TouchableOpacity>
          </View>
        </Content>
        <ButtonActionSpinner
          ref={ref => (this.actionButtonRef = ref)}
          action={this.login}
          text="SignIn"
          isLoading={loading}
          buttonStyle={{
            backgroundColor: '#e22f5b',
            position: 'absolute',
            bottom: 100,
            right: 0,
            zIndex: 99,
          }}
        />

        <Content />
      </Container>
    );
  }
}

export default LoginForm;
