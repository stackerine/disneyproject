import React from 'react';
import { Animated } from 'react-native';
import { Svg, Path } from 'react-native-svg';

const AnimatedPath = Animated.createAnimatedComponent(Path);

const AnimatedWave = ({ width, height, path }) => {
  return (
    <Svg width={width} height={height} style={{ zIndex: 2 }}>
      <AnimatedPath d={path} fill="white" />
    </Svg>
  );
};

export default AnimatedWave;
