import React, { Component } from 'react';
import { View } from 'react-native';
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  H1,
  Toast,
} from 'native-base';

import { isValidEmail, isValidPassword } from 'commons/formVerification';
import ButtonActionSpinner from '../../library/molecules/ButtonActionSpinner';

const FORM_INPUT_STATES = {
  normal: { error: false, success: false },
  error: { error: true, success: false },
  success: { error: false, success: true },
};

class SignUpForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      passwordConf: '',
      emailValidationStates: FORM_INPUT_STATES.normal,
      passwordValidationStates: FORM_INPUT_STATES.normal,
      passwordConfValidationStates: FORM_INPUT_STATES.normal,
    };
    this.passwordRef = null;
    this.pwdConfRef = null;
    this.actionButtonRef = null;
  }

  componentDidUpdate({ authError: prevAuthError, user: prevUser }) {
    const { email } = this.state;
    const { authError, user, isActive } = this.props;

    if (isActive) {
      if (authError && prevAuthError !== authError) {
        Toast.show({
          text: authError,
          buttonText: 'Try Again',
          type: 'danger',
          duration: 1000 * 60, // 1 minute,
          onClose: this.actionButtonRef.makeAction,
        });
      } else if (user && prevUser !== user) {
        this.props.signupSuccess();
      }
    }
  }

  updateLogin = (field, text) => {
    this.setState({ [field]: text });
  };

  checkEmail = () => {
    const isGoodEmail = isValidEmail(this.state.email);
    if (isGoodEmail) {
      this.setState({
        emailValidationStates: FORM_INPUT_STATES.success,
      });
    } else {
      this.setState({
        emailValidationStates: FORM_INPUT_STATES.error,
      });
      Toast.show({
        text: 'Please enter a valid email',
        buttonText: 'Try again',
        type: 'danger',
        duration: 3000,
      });
    }
    return isGoodEmail;
  };

  checkPassword = () => {
    const isGoodPassword = isValidPassword(this.state.password);

    if (isGoodPassword) {
      this.setState({
        passwordValidationStates: FORM_INPUT_STATES.success,
      });
    } else {
      this.setState({
        passwordValidationStates: FORM_INPUT_STATES.error,
      });
      Toast.show({
        text: 'Password must be 4-18 characters.',
        buttonText: 'Try again',
        type: 'danger',
        duration: 3000,
      });
    }
    return isGoodPassword;
  };

  checkPasswordConf = () => {
    const isGoodPasswordConf = this.state.password === this.state.passwordConf;

    if (isGoodPasswordConf) {
      this.setState({
        passwordConfValidationStates: FORM_INPUT_STATES.success,
      });
    } else {
      this.setState({
        passwordConfValidationStates: FORM_INPUT_STATES.error,
      });
      Toast.show({
        text: 'You have to enter the same password 2 times',
        buttonText: 'Try again',
        type: 'danger',
        duration: 3000,
      });
    }
    return isGoodPasswordConf;
  };

  focusOnPassword = () => {
    if (this.checkEmail()) {
      this.passwordRef._root.focus();
    }
  };

  focusOnPwdConfirmation = () => {
    if (this.checkPassword()) {
      this.pwdConfRef._root.focus();
    }
  };

  validateAccountInformation = () => {
    const isGoodEmail = this.checkEmail();
    const isGoodPassword = this.checkPassword();
    const isSamePassword = this.checkPasswordConf();

    return isGoodEmail && isGoodPassword && isSamePassword;
  };

  onSubmit = () => {
    this.actionButtonRef.makeAction();
  };

  signup = async () => {
    const { email, password } = this.state;
    const isValideInformations = this.validateAccountInformation();

    if (isValideInformations) {
      await this.props.signUpUser({ email, password });
    }
  };

  render() {
    const { height, width } = this.props;
    const {
      email,
      password,
      passwordConf,
      emailValidationStates,
      passwordValidationStates,
      passwordConfValidationStates,
    } = this.state;
    const containerLayout = {
      position: 'absolute',
      height: height,
      width: width,
    };

    return (
      <Container style={containerLayout}>
        <Content style={{ ...containerLayout, zIndex: 1 }}>
          <H1 style={{ textAlign: 'center' }}>Signup</H1>
          <Form>
            <Item last floatingLabel {...emailValidationStates}>
              <Label>Email</Label>
              <Input
                textContentType="username"
                value={email}
                onChangeText={this.updateLogin.bind(null, 'email')}
                returnKeyType="next"
                onSubmitEditing={this.focusOnPassword}
              />
            </Item>
            <Item last floatingLabel {...passwordValidationStates}>
              <Label>Password</Label>
              <Input
                getRef={ref => (this.passwordRef = ref)}
                secureTextEntry={true}
                textContentType="password"
                value={password}
                onChangeText={this.updateLogin.bind(null, 'password')}
                returnKeyType="next"
                onSubmitEditing={this.focusOnPwdConfirmation}
              />
            </Item>
            <Item last floatingLabel {...passwordConfValidationStates}>
              <Label>Password Confirmation</Label>
              <Input
                getRef={ref => (this.pwdConfRef = ref)}
                secureTextEntry={true}
                textContentType="password"
                value={passwordConf}
                onChangeText={this.updateLogin.bind(null, 'passwordConf')}
                returnKeyType="next"
                onSubmitEditing={this.onSubmit}
              />
            </Item>
          </Form>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'flex-end',
            }}
          />
        </Content>
        <ButtonActionSpinner
          ref={ref => (this.actionButtonRef = ref)}
          action={this.signup}
          text="SignUp"
          buttonStyle={{
            backgroundColor: '#4D32A5',
            position: 'absolute',
            bottom: 100,
            right: 0,
            zIndex: 99,
          }}
        />
        <Content />
      </Container>
    );
  }
}

export default SignUpForm;
