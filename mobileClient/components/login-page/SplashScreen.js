import React, { Component } from 'react';
import { Platform, Linking, View, ActivityIndicator, Text } from 'react-native';

import { isVerifiedUser } from '../../services/auth';
import {
  isVerificationEmailUrl,
  getVerificationEmailOobCode,
} from '../../utils/url';

const LOADING_MESSAGES = {
  APP_LOADING: 'Baaap is loading...',
  EMAIL_VERIFICATION: 'Please wait during email verification...',
};

export default class SplashScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      message: LOADING_MESSAGES.APP_LOADING,
    };

    this.linkingUnsubscriber = null;
  }
  componentDidMount() {
    this.start();
  }

  componentWillUnmount() {
    this.linkingUnsubscriber && this.linkingUnsubscriber();
  }

  componentDidUpdate({ error: prevError, status: prevStatus }) {
    const { error, status } = this.props;
    if (error && prevError !== error) {
      this.props.navigation.navigate('AuthScreen', {
        message: { type: 'danger', text: error },
      });
    } else if (status && prevStatus !== status) {
      this.props.navigation.navigate('AuthScreen', {
        message: { type: 'success', text: status },
      });
    }
  }

  start = async () => {
    if (Platform.OS === 'android') {
      const url = await Linking.getInitialURL();
      await this.manageDeepLinkWithUrl(url);
    } else {
      this.linkingUnsubscriber = Linking.addEventListener('url', ({ url }) =>
        this.manageDeepLinkWithUrl(url)
      );
    }
  };

  manageDeepLinkWithUrl = async url => {
    if (url) {
      if (isVerificationEmailUrl(url)) {
        const oobCode = getVerificationEmailOobCode(url);

        this.setState({
          message: LOADING_MESSAGES.EMAIL_VERIFICATION,
        });
        this.props.emailVerificationWithOobCode(oobCode);

        return true;
      }
    }
    this.checkAuth();
  };

  checkAuth = async () => {
    const isAuth = await isVerifiedUser();

    if (isAuth) {
      this.props.navigation.navigate('App');
    } else {
      this.props.navigation.navigate('AuthScreen');
    }
  };

  render() {
    const { message } = this.state;

    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <Text>{message}</Text>
        <ActivityIndicator color="blue" size="large" />
      </View>
    );
  }
}
