import React from 'react';
import { View, Text, Animated } from 'react-native';

const LoginSignupButtons = ({
  width,
  height,
  changeActiveSignMode,
  isLoginModeSelected,
  logButtonAppearValue,
}) => {
  return (
    <View
      style={{
        position: 'absolute',
        width,
        height,
      }}
    >
      <Animated.Text
        onPress={changeActiveSignMode.bind(null, true)}
        style={{
          position: 'absolute',
          zIndex: 999,
          fontSize: 30,
          fontWeight: 'bold',
          color: isLoginModeSelected ? 'white' : '#841a34',
          top: height / 6 - 10,
          left: width / 8,
          transform: [
            { rotate: '-15deg' },
            {
              translateY: logButtonAppearValue,
            },
          ],
        }}
      >
        Login
      </Animated.Text>
      <Animated.Text
        onPress={changeActiveSignMode.bind(null, false)}
        style={{
          position: 'absolute',
          zIndex: 999,
          fontSize: 30,
          fontWeight: 'bold',
          color: isLoginModeSelected ? '#211548' : 'white',
          top: height / 6 - 10,
          right: width / 8,
          transform: [
            { rotate: '15deg' },
            {
              translateY: logButtonAppearValue,
            },
          ],
        }}
      >
        Signup
      </Animated.Text>
    </View>
  );
};

export default LoginSignupButtons;
