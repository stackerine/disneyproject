import { connect } from 'react-redux';
import LoginForm from '../LoginForm';

import { signInUser } from '../../../actions/auth';

const mapStateToProps = ({ auth }) => {
  const { error, loading, user } = auth;

  return { authError: error, loading, user };
};

const mapDispatchToProps = {
  signInUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginForm);
