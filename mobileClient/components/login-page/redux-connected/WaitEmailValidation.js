import { connect } from 'react-redux';
import WaitEmailValidationPage from '../WaitEmailValidationPage';

import {
  resendVerificationEmail,
  clearState,
} from '../../../actions/emailActions';

const mapStateToProps = ({ emailActions: { sendEmailVerification } }) => {
  const { error, loading, status } = sendEmailVerification;

  return { error, loading, status };
};

const mapDispatchToProps = {
  resendVerificationEmail,
  clearState,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(WaitEmailValidationPage);
