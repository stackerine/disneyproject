import { connect } from 'react-redux';
import SignUpForm from '../SignUpForm';

import { signUpUser } from '../../../actions/auth';

const mapStateToProps = ({ auth }) => {
  const { error, loading, user } = auth;

  return { authError: error, loading, user };
};

const mapDispatchToProps = {
  signUpUser,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SignUpForm);
