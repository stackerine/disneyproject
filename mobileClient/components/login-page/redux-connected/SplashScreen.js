import { connect } from 'react-redux';
import SplashScreen from '../SplashScreen';

import { emailVerificationWithOobCode } from '../../../actions/emailActions';

const mapStateToProps = ({ emailActions: { emailVerification } }) => {
  const { error, loading, status } = emailVerification;

  return { error, loading, status };
};

const mapDispatchToProps = {
  emailVerificationWithOobCode,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SplashScreen);
