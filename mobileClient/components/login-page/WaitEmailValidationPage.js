import React, { Component } from 'react';
import { View } from 'react-native';
import { H1, Text as NBText, Button, Icon, Toast } from 'native-base';

export default class WaitEmailValidationPage extends Component {
  sendValidationEmail = async () => {
    this.props.resendVerificationEmail();
  };

  componentDidUpdate(prevProps) {
    const { error, status, clearState } = this.props;
    const { type, text } = error
      ? { type: 'danger', text: error }
      : { type: 'success', text: status };

    if (error || status) {
      Toast.show({
        type,
        text,
      });
    }
  }

  goToSigninPage = (params = {}) => {
    this.props.clearState();
    this.props.navigation.navigate('AuthScreen', params);
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          padding: 30,
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <View style={{ marginVertical: 20 }}>
          <Icon
            type="FontAwesome5"
            name="check-circle"
            style={{ fontSize: 150, color: '#3F51B5' }}
          />
        </View>
        <View style={{ marginVertical: 20, alignItems: 'center' }}>
          <H1>Email not verified </H1>
          <NBText>
            We sent you an email with instruction on how to activate your
            account. Check your inbox and get started with Baaap application.
          </NBText>
        </View>
        <View />
        <View style={{ alignItems: 'center' }}>
          <Button bordered rounded onPress={this.sendValidationEmail}>
            <NBText>Send me another email, please.</NBText>
          </Button>
          <Button bordered rounded onPress={this.goToSigninPage}>
            <NBText>Go To Login</NBText>
          </Button>
        </View>
      </View>
    );
  }
}
