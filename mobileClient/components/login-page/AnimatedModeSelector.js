import React from 'react';
import { Animated } from 'react-native';
import { Svg, G, Path } from 'react-native-svg';

const AnimatedG = Animated.createAnimatedComponent(G);
const AnimatedPath = Animated.createAnimatedComponent(Path);

const AnimatedModeSelector = React.forwardRef(
  (
    {
      width,
      height,
      logButtonDashAppearValue,
      loginPath,
      isSecondLoginAnimationStart,
      isLoginModeSelected,
    },
    ref
  ) => {
    const [pivotX, pivotY] = [0, height * 2];
    const dashColor = isSecondLoginAnimationStart
      ? isLoginModeSelected
        ? '#03cb9d'
        : '#4D32A5'
      : 'transparent';

    return (
      <Animated.View style={{ zIndex: 3 }}>
        <Svg
          viewBox={`-${pivotX} -${pivotY} ${width} ${height}`}
          width={width}
          height={height}
          style={{ zIndex: 2, position: 'absolute' }}
        >
          <AnimatedG
            style={{
              transform: [{ rotate: logButtonDashAppearValue }],
            }}
          >
            <AnimatedPath
              ref={ref}
              d={loginPath}
              fill="none"
              stroke={dashColor}
              strokeWidth="10"
              strokeDashoffset="0"
              strokeDasharray={[width / 2, width]}
              transform={`translate(${-pivotX} ${-pivotY})`}
            />
          </AnimatedG>
        </Svg>
      </Animated.View>
    );
  }
);

export default AnimatedModeSelector;
