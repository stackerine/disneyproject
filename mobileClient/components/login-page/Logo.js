import React from 'react';
import { View } from 'react-native';

const Logo = () => (
  <View
    style={{
      backgroundColor: 'rgba(231,76,60,1)',
      borderRadius: 50,
      width: 70,
      height: 70,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 12,
      },
      shadowOpacity: 1,
      shadowRadius: 5.0,

      elevation: 24,
    }}
  />
);

export default Logo;
