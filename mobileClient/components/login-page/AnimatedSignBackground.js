import React from 'react';
import { Animated } from 'react-native';

const AnimatedSignBackground = ({
  width,
  height,
  color,
  zIndex,
  backgroundSwitchColorValue,
}) => {
  return (
    <Animated.View
      style={{
        position: 'absolute',
        zIndex,
        width: width,
        height: height,
        backgroundColor: color,
        transform: [
          {
            translateX: backgroundSwitchColorValue,
          },
        ],
      }}
    />
  );
};

export default AnimatedSignBackground;
