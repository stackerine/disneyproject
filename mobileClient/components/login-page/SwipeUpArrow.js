import React, { Component } from 'react';
import { Text, Animated, PanResponder } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const SwipeUpArrow = ({
  width,
  height,
  opacity,
  isSecondPhaseLaunched,
  startSecondPhase,
}) => {
  const arrowAnim = new Animated.Value(0);
  const dragValueY = new Animated.Value(0);

  const heightLimit = height / 4;
  const lastDragYValue = 0;

  Animated.loop(
    Animated.timing(arrowAnim, {
      useNativeDriver: true,
      toValue: 1,
      duration: 1000,
    })
  ).start();

  const arrowValue = arrowAnim.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [0, 20, 0],
  });

  const dragValue = dragValueY.interpolate({
    inputRange: [0, 200],
    outputRange: [0, 200],
  });

  dragValueY.addListener(({ value }) => {
    this.lastDragYValue = value;
  });
  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: (e, gestureState) => true,
    onMoveShouldSetPanResponder: (e, gestureState) => true,
    onPanResponderGrant: (e, gestureState) => {
      dragValueY.setOffset(lastDragYValue);
      dragValueY.setValue(0);
    },
    onPanResponderMove: (e, gestureState) => {
      const newValue = gestureState.dy;
      const limitedValue = Math.min(Math.max(newValue, -heightLimit), 0);
      dragValueY.setValue(newValue);

      if (limitedValue < -heightLimit + 50 && !isSecondPhaseLaunched) {
        startSecondPhase();
      }
    },
    onPanResponderRelease: (e, gestureState) => {},
  });

  return (
    <Animated.View
      {...panResponder.panHandlers}
      style={{
        top: height - 200,
        zIndex: 99,
        position: 'absolute',
        transform: [
          {
            translateY: dragValue,
          },
        ],
      }}
    >
      <Animated.View
        style={{
          alignItems: 'center',
          opacity: opacity,
          transform: [
            {
              translateY: arrowValue,
            },
          ],
          width,
        }}
      >
        <Icon name="arrow-up" size={30} color="#4D32A5" />
        <Text style={{ color: '#4D32A5' }}>Swipe Up</Text>
        <Text style={{ color: '#4D32A5' }}>to start...</Text>
      </Animated.View>
    </Animated.View>
  );
};

export default SwipeUpArrow;
