import React, { Component } from 'react';
import { View, Text, Animated } from 'react-native';

import LoginForm from './redux-connected/SignIn';
import SignUpForm from './redux-connected/SignUp';

import { isVerifiedUser } from '../../services/auth';

const LoginSignupForm = ({
  animValue,
  width,
  height,
  redirectToPage,
  isLoginModeSelected,
}) => {
  const loginSuccess = () => {
    redirectToPage(isVerifiedUser() ? 'App' : 'WaitEmailValidation');
  };
  const signupSuccess = () => redirectToPage('WaitEmailValidation');

  const calculatedHeight = (4 * height) / 5 - 100;
  const formLoginSignupFormValue = animValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -width],
  });

  return (
    <Animated.View
      style={{
        flexDirection: 'row',
        transform: [{ translateX: formLoginSignupFormValue }],
      }}
    >
      <View
        style={{
          height: calculatedHeight,
          width: width,
        }}
      >
        <LoginForm
          height={calculatedHeight}
          width={width}
          loginSuccess={loginSuccess}
          isActive={isLoginModeSelected}
        />
      </View>
      <View style={{ width }}>
        <SignUpForm
          height={calculatedHeight}
          width={width}
          signupSuccess={signupSuccess}
          isActive={!isLoginModeSelected}
        />
      </View>
    </Animated.View>
  );
};

export default LoginSignupForm;
