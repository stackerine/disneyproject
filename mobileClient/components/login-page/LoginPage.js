import React, { Component } from 'react';
import { View, Dimensions, Animated, Easing } from 'react-native';
import { Toast } from 'native-base';
import LoginSignupForm from './LoginSignupForm';

import SwipeUpArrow from './SwipeUpArrow';
import LoginSignupButtons from './LoginSignupButtons';
import AnimatedWave from './AnimatedWave';
import AnimatedModeSelector from './AnimatedModeSelector';
import Logo from './Logo';
import AnimatedSignBackground from './AnimatedSignBackground';

export default class LoginPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isSecondPhaseLaunched: false,
      isSecondLoginAnimationStart: false,
      isLoginModeSelected: true,
    };

    this.backgroundSwitchColorAnim = new Animated.Value(0.01); // Background color anim on log switch mode
    this.formAppearAnim = new Animated.Value(0.01); // Appear of form
    this.formSwitchAnim = new Animated.Value(0.01); // Anim of form switch
    this.dashSwitchAnim = new Animated.Value(0.01); // Anim of dash under log / sign buttons
    this.loginSignupDashRef = React.createRef(); // Ref to update dash SVG
    this.logButtonsAppearAnim = new Animated.Value(0.01); // Appear of log button
    this.logButtonDashAppearAnim = new Animated.Value(0); // Appear of dash under log button
    this.logoAppearAnim = new Animated.Value(0.01); // Logo appear
    this.arrowOpacityAnim = new Animated.Value(1); // Arrow opacity
    this.waveShapeAnim = new Animated.Value(0); // Wave shape anim

    const { width, height } = Dimensions.get('window');
    this.width = width;
    this.height = height;
  }

  componentDidMount() {
    this.startListenDashAnimValue();
    this.managePageMessages();
  }

  /**
   * This function show Toast that came from
   * other pages
   */
  managePageMessages = () => {
    const { navigation } = this.props;
    const message = navigation.getParam('message');

    if (message) {
      Toast.show(message);
    }
  };

  startListenDashAnimValue = () => {
    this.dashSwitchAnim.addListener(animValue => {
      if (animValue.value !== 1) {
        this.loginSignupDashRef.setNativeProps({
          strokeDashoffset: 0,
          strokeDasharray: [this.width / 2, this.width],
        });
      } else {
        const bezierOffset = this.height / 5;
        const arcSize = Math.sqrt(
          Math.pow(this.width / 2, 2) + Math.pow(bezierOffset * 0.5, 2)
        );
        this.loginSignupDashRef.setNativeProps({
          strokeDashoffset: arcSize,
          strokeDasharray: [arcSize, arcSize],
        });
      }
    });
  };

  startSecondPhase = () => {
    this.setState({ isSecondPhaseLaunched: true });

    this.startWaveShapeAnimation();
    this.startArrowOpacityAnimation();
    this.startLogoAppearAnimation();
    this.startLoginDashAppearAnimation();
    this.startLogButtonAppearAnimation();
    this.startFormAppearAnimation();
  };

  startWaveShapeAnimation = () => {
    Animated.spring(this.waveShapeAnim, {
      useNativeDriver: false,
      toValue: 1,
      duration: 2000,
      easing: Easing.bounce,
      bounciness: 10,
      speed: 1,
    }).start();
  };

  startArrowOpacityAnimation = () => {
    Animated.timing(this.arrowOpacityAnim, {
      useNativeDriver: true,
      toValue: 0,
      duration: 200,
      easing: Easing.ease,
    }).start();
  };

  startLogoAppearAnimation = () => {
    Animated.timing(this.logoAppearAnim, {
      useNativeDriver: true,
      toValue: 1,
      duration: 200,
      easing: Easing.ease,
      delay: 500,
    }).start();
  };

  startLoginDashAppearAnimation = () => {
    const delay = 500;
    Animated.timing(this.logButtonDashAppearAnim, {
      useNativeDriver: true,
      toValue: 1,
      duration: 250,
      easing: Easing.ease,
      delay,
    }).start(() =>
      this.setState({
        isSecondLoginAnimationStart: true,
      })
    );
  };

  startBackgroundSwitchColorAnimation = isLoginModeSelected => {
    Animated.timing(this.backgroundSwitchColorAnim, {
      useNativeDriver: true,
      toValue: isLoginModeSelected ? 0.01 : 1,
      duration: 250,
      easing: Easing.linear,
    }).start();
  };

  startLogButtonAppearAnimation = () => {
    Animated.spring(this.logButtonsAppearAnim, {
      useNativeDriver: true,
      toValue: 1,
      bounciness: 10,
      speed: 1,
    }).start();
  };

  startDashAnimation = isLoginModeSelected => {
    const bezierOffset = this.height / 5;
    const arcSize = Math.sqrt(
      Math.pow(this.width / 2, 2) + Math.pow(bezierOffset * 0.5, 2)
    );

    const loginStyleDash = {
      strokeDashoffset: 0,
      strokeDasharray: [this.width / 2, this.width],
    };

    const signupStyleDash = {
      strokeDashoffset: arcSize,
      strokeDasharray: [arcSize, arcSize],
    };

    const dashStyles = isLoginModeSelected ? loginStyleDash : signupStyleDash;

    this.loginSignupDashRef.current.setNativeProps(dashStyles);
  };

  startFormAppearAnimation = () => {
    Animated.timing(this.formAppearAnim, {
      useNativeDriver: true,
      toValue: 1,
      duration: 350,
      easing: Easing.ease,
    }).start();
  };

  startFormSwitchAnimation = isLoginModeSelected => {
    Animated.timing(this.formSwitchAnim, {
      useNativeDriver: false,
      toValue: isLoginModeSelected ? 0.01 : 1,
      duration: 250,
      easing: Easing.ease,
    }).start();
  };

  changeActiveSignMode = isLoginModeSelected => {
    this.setState({
      isLoginModeSelected,
    });
    this.startBackgroundSwitchColorAnimation(isLoginModeSelected);
    this.startDashAnimation(isLoginModeSelected);
    this.startFormSwitchAnimation(isLoginModeSelected);
  };

  redirectToPage = (pageName, props = {}) => {
    this.props.navigation.navigate(pageName, props);
  };

  render() {
    const {
      isSecondLoginAnimationStart,
      isLoginModeSelected,
      isSecondPhaseLaunched,
    } = this.state;
    let { width, height } = Dimensions.get('window');
    width = Math.round(width);
    height = Math.round(height);

    const bezierOffset = height / 5;

    const aaStart = { x: width, y: Math.round(height / 2) };
    const aStart = {
      x: width - bezierOffset,
      y: Math.round(height / 2 - bezierOffset * 0.5),
    };
    const bStart = {
      x: bezierOffset,
      y: Math.round(height / 2 - bezierOffset * 0.5),
    };
    const cStart = { x: 0, y: Math.round(height / 2) };

    const startPath = `
    M 0 ${height}
    L ${width} ${height}
    L ${aaStart.x} ${aaStart.y}
    C ${aStart.x},${aStart.y} ${bStart.x},${bStart.y} ${cStart.x},${cStart.y} 
    Z`;

    const aaEnd = { x: width, y: Math.round(height / 2 - bezierOffset) };
    const aEnd = {
      x: width - bezierOffset,
      y: Math.round(height / 2 - bezierOffset * 1.5),
    };
    const bEnd = {
      x: bezierOffset,
      y: Math.round(height / 2 - bezierOffset * 1.5),
    };
    const cEnd = { x: 0, y: Math.round(height / 2 - bezierOffset) };

    const endPath = `
    M 0 ${height}
    L ${width} ${height}
    L ${aaEnd.x} ${aaEnd.y}
    C ${aEnd.x},${aEnd.y} ${bEnd.x},${bEnd.y} ${cEnd.x},${cEnd.y} 
    Z`;

    const aaMiddle = { x: width, y: Math.round(height / 2 - bezierOffset) };
    const aMiddle = {
      x: width - bezierOffset,
      y: Math.round(height / 2 - bezierOffset * 2),
    };
    const bMiddle = {
      x: bezierOffset,
      y: Math.round(height / 2 - bezierOffset * 2),
    };
    const cMiddle = { x: 0, y: Math.round(height / 2 - bezierOffset) };

    const middlePath = `
    M 0 ${height}
    L ${width} ${height}
    L ${aaMiddle.x} ${aaMiddle.y}
    C ${aMiddle.x},${aMiddle.y} ${bMiddle.x},${bMiddle.y} ${cMiddle.x},${
      cMiddle.y
    } 
    Z`;

    const loginPath = `
    M ${cEnd.x} ${cEnd.y}
    C ${bEnd.x},${bEnd.y} ${aEnd.x},${aEnd.y} ${aaEnd.x},${aaEnd.y}`;

    const path = this.waveShapeAnim.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [startPath, middlePath, endPath],
    });

    const logButtonDashAppearValue = this.logButtonDashAppearAnim.interpolate({
      inputRange: [0, 1],
      outputRange: ['-90deg', '0deg'],
    });

    const backgroundSwitchColorValue = this.backgroundSwitchColorAnim.interpolate(
      {
        inputRange: [0, 1],
        outputRange: [-width - 10, 0],
      }
    );

    const logButtonAppearValue = this.logButtonsAppearAnim.interpolate({
      inputRange: [0, 1],
      outputRange: [height, 0],
    });

    const formAppearValue = this.formAppearAnim.interpolate({
      inputRange: [0, 1],
      outputRange: [height, height / 5 + 100],
    });

    return (
      <View style={{ backgroundColor: '#4D32A5', flex: 1 }}>
        <AnimatedSignBackground
          width={width}
          height={height}
          color="#e22f5b"
          zIndex={1}
          backgroundSwitchColorValue={backgroundSwitchColorValue}
        />

        <Animated.View
          style={{
            position: 'absolute',
            top: Math.round(height / 2 - bezierOffset * 1.5),
            width: width,
            height: 56,
            elevation: 2,
            transform: [
              { scaleX: this.logoAppearAnim },
              { scaleY: this.logoAppearAnim },
            ],
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Logo />
        </Animated.View>

        <SwipeUpArrow
          width={width}
          height={height}
          opacity={this.arrowOpacityAnim}
          isSecondPhaseLaunched={isSecondPhaseLaunched}
          startSecondPhase={this.startSecondPhase}
        />

        <LoginSignupButtons
          width={width}
          height={height}
          changeActiveSignMode={this.changeActiveSignMode}
          isLoginModeSelected={isLoginModeSelected}
          logButtonAppearValue={logButtonAppearValue}
        />
        <AnimatedModeSelector
          ref={this.loginSignupDashRef}
          width={width}
          height={height}
          logButtonDashAppearValue={logButtonDashAppearValue}
          loginSignupDashRef={this.loginSignupDashRef}
          loginPath={loginPath}
          isSecondLoginAnimationStart={isSecondLoginAnimationStart}
          isLoginModeSelected={isLoginModeSelected}
        />
        <AnimatedWave width={width} height={height} path={path} />

        <Animated.View
          style={{
            position: 'absolute',
            transform: [
              {
                translateY: formAppearValue,
              },
            ],
            zIndex: 3,
          }}
        >
          <LoginSignupForm
            animValue={this.formSwitchAnim}
            width={width}
            height={height}
            redirectToPage={this.redirectToPage}
            isLoginModeSelected={isLoginModeSelected}
          />
        </Animated.View>
      </View>
    );
  }
}
