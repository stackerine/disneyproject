import React, { Component } from 'react';
import { View, Button, Text, FlatList } from 'react-native';

import {
  AppPage,
  AppCategoryTitles,
  AppBlock,
  AppLogo,
  AppTitle,
  AppListSeparator,
} from './style/appScreenStyle.js';

import { getUser } from '../../services/auth';

class AppScreen extends Component {
  state = {
    userId: '',
  };

  componentDidMount() {
    this.getUserId();
  }

  getUserId = async () => {
    const { uid: userId } = await getUser();
    this.setState({ userId });
  };

  clearSession = async () => {
    this.props.signOutUser();
    this.props.navigation.navigate('AuthScreen');
  };

  createNewApplication = async () => {
    this.props.navigation.navigate('CreateApp');
  };

  goEditAppPage = (id, title) =>
    this.props.navigation.navigate('EditApp', {
      appId: id,
      title: title,
    });

  render() {
    const { userId } = this.state;
    const { applications } = this.props;

    const showApps = (
      <FlatList
        data={applications}
        keyExtractor={item => item.id}
        renderItem={({ item }) => (
          <AppBlock>
            <AppLogo
              resizeMode="contain"
              source={{
                uri:
                  'https://www.myfrenchstartup.com/logo/tricy_application_icon.png',
              }}
            />
            {/*TODO SAVE LOGO FOR APP AND REPLACE IMAGE LINK WITH IT*/}

            <View>
              <AppTitle>{item.title}</AppTitle>
              {item.description && <Text>{item.description}</Text>}
              {item.maintainers && item.maintainers.includes(userId) && (
                <Button
                  title="edit app"
                  onPress={this.goEditAppPage.bind(null, item.id, item.title)}
                />
              )}
            </View>
          </AppBlock>
        )}
        ItemSeparatorComponent={() => <AppListSeparator />}
      />
    );

    return (
      <View style={{ flex: 1 }}>
        <Button title="logout" onPress={this.clearSession} />
        <Button
          title="Create new application"
          onPress={this.createNewApplication}
          color="cadetblue"
        />
        <AppPage>
          <AppCategoryTitles>My applications</AppCategoryTitles>
          <View>{showApps}</View>
        </AppPage>
      </View>
    );
  }
}

export default AppScreen;
