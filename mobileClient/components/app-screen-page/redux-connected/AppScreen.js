import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { getUser } from '../../../services/auth';
import AppScreen from '../AppScreen';

import { signOutUser } from '../../../actions/auth';

const mapStateToProps = state => ({
  applications: state.firestore.ordered.apps,
});

const mapDispatchToProps = {
  signOutUser,
};

export default compose(
  firestoreConnect(props => {
    return [
      {
        collection: 'apps',
        where: [['subscribers', 'array-contains', getUser().uid]],
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(AppScreen);
