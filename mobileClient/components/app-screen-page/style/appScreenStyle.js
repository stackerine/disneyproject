import styled from 'styled-components';
import { ScrollView, View, Image, Text } from 'react-native';

export const AppPage = styled(ScrollView)`
  margin: 10px;
`;

export const AppCategoryTitles = styled(Text)`
  text-transform: uppercase;
  font-weight: 900;
  margin: auto;
`;

export const AppsContainer = styled(View)`
  flex-direction: row;
  flex-wrap: wrap;
  margin-bottom: 20px;
`;

export const AppBlock = styled(View)`
  margin: 10px 0 10px 0;
  flex-direction: row;
  align-items: center;
`;

export const AppLogo = styled(Image)`
  margin: 0 10px 0 0;
  width: 40;
  height: 40;
`;

export const AppTitle = styled(Text)`
  font-size: 15;
  font-weight: 900;
`;

export const AppListSeparator = styled(View)`
  background-color: silver;
  height: 1;
  margin: 0;
`;
