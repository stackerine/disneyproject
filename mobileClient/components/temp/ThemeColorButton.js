import React, { Fragment } from 'react';
import { Button, Text } from 'native-base';

const ThemeColorButton = ({ title, changeTheme }) => {
  return (
    <Fragment>
      <Button onPress={changeTheme.bind(null, title)}>
        <Text>{title}</Text>
      </Button>
    </Fragment>
  );
};

export default ThemeColorButton;
