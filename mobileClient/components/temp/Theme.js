import React, { Component } from 'react';
import { ButtonThemed, TextThemed } from '../../style/themes';
import { Container, Content, Button, Text, Badge } from 'native-base';
import { toggleTheme } from '../../style/themes/';
import ThemeColorButton from './ThemeColorButton';
import { changeThemeApplication } from '../../services/firebase';

class Theme extends Component {
  onPressChangeThemeButton = async theme => {
    return await changeThemeApplication(theme);
  };

  render() {
    // TODO : page de test, la supprimer plus déplacer la fonctionnalité de changement de thème dans les paramètres de l'application
    return (
      <Container>
        <Content>
          <ThemeColorButton
            changeTheme={this.onPressChangeThemeButton}
            title="material"
          />
          <ThemeColorButton
            changeTheme={this.onPressChangeThemeButton}
            title="commonColor"
          />
          <ThemeColorButton
            changeTheme={this.onPressChangeThemeButton}
            title="platform"
          />
        </Content>
      </Container>
    );
  }
}

export default Theme;
