import React, { Component } from 'react';
import {
  Container,
  Content,
  Text,
  Form,
  Item,
  Label,
  Input,
  Icon,
} from 'native-base';
import ButtonActionSpinner from '../../library/molecules/ButtonActionSpinner';
import { withFirebase } from 'react-redux-firebase';
import { LIGHT_GREY } from '../../style/colors';

class InfoElem extends Component {
  state = {
    text: this.props.navigation.getParam('dataDisplayed', ''),
  };

  componentDidUpdate({ user: prevUser, error: prevError }) {
    const { user, error } = this.props;

    if (prevUser !== user && !error) {
      this.backToAccountMenu();
    }
  }

  updateInfo = async (dataToUpdate, value) => {
    await this.props.updateUserProfile(
      { field: dataToUpdate, value },
      this.props.firebase
    );
  };

  backToAccountMenu = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  render() {
    const { text } = this.state;
    const { navigation, error, loading } = this.props;
    const placeholder = navigation.getParam('placeholder', 'New value');
    const objectKey = navigation.getParam('objectKey', '');
    const feedbackErrorIcon = !!error && <Icon name="close-circle" />;
    return (
      <Container>
        <Content padder>
          <Form>
            <Item fixedLabel error={!!error}>
              <Label>{placeholder}</Label>
              <Input
                placeholder={placeholder}
                onChangeText={text => this.setState({ text })}
                placeholderTextColor={LIGHT_GREY}
                value={text}
              />
              {feedbackErrorIcon}
            </Item>
            <Text>{error}</Text>
          </Form>
          <ButtonActionSpinner
            action={this.updateInfo.bind(this, objectKey, text)}
            text="Save"
            isLoading={loading}
            buttonStyle={{
              backgroundColor: '#e22f5b',
            }}
          />
        </Content>
      </Container>
    );
  }
}

export default withFirebase(InfoElem);
