import React, { Component } from 'react';
import { Container, Content, Spinner, List } from 'native-base';
import { logout, getUser } from '../../services/auth';
import ItemList from './ItemList';

export default class AccountPage extends Component {
  goToUserInfo = params => {
    this.props.navigation.navigate('InfoElem', {
      ...params,
    });
  };

  disconnection = async () => {
    await logout();
    this.props.navigation.navigate('AuthScreen');
  };

  render() {
    const {
      user: { email, displayName, phoneNumber, photoURL },
    } = this.props;

    const userInfo = (
      <Content>
        <List>
          <ItemList
            title="Username"
            leftIcon="contact"
            rightIcon="arrow-forward"
            color="#3498db"
            action={this.goToUserInfo.bind(this, {
              title: 'Username',
              placeholder: 'New username',
              objectKey: 'displayName',
              dataDisplayed: displayName,
            })}
            data={displayName}
          />
          <ItemList
            title="Password"
            leftIcon="key"
            rightIcon="arrow-forward"
            color="#3498db"
            action={this.goToUserInfo.bind(this, {
              data: '*****',
              title: 'Password',
              placeholder: 'New password',
              objectKey: 'password',
              dataDisplayed: '*****',
            })}
            data="*****"
          />
          <ItemList
            title="Email"
            leftIcon="mail"
            rightIcon="arrow-forward"
            color="#3498db"
            action={this.goToUserInfo.bind(this, {
              data: email,
              title: 'Email',
              placeholder: 'New email',
              objectKey: 'email',
              dataDisplayed: `${email.slice(0, 5)}*****`,
            })}
            data={`${email.slice(0, 5)}*****`}
          />
          <ItemList
            title="Phone number"
            leftIcon="call"
            rightIcon="arrow-forward"
            color="#3498db"
            action={this.goToUserInfo.bind(this, {
              title: 'Phone number',
              placeholder: 'New phone number',
              objectKey: 'phoneNumber',
              dataDisplayed:
                phoneNumber &&
                `*****${phoneNumber.slice(phoneNumber.length - 2)}`,
            })}
            data={
              phoneNumber && `*****${phoneNumber.slice(phoneNumber.length - 2)}`
            }
          />
          <ItemList
            title="Disconnection"
            leftIcon="close"
            color="#e74c3c"
            action={this.disconnection}
          />
        </List>
      </Content>
    );
    return <Container>{userInfo}</Container>;
  }
}
