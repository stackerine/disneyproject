import React from 'react';
import { Text, ListItem, Left, Right, Icon, Button, Body } from 'native-base';

const ItemList = ({ title, leftIcon, rightIcon, color, action, data }) => {
  return (
    <ListItem icon onPress={action}>
      <Left>
        <Button style={{ backgroundColor: color }}>
          <Icon active name={leftIcon} />
        </Button>
      </Left>
      <Body>
        <Text>{title}</Text>
      </Body>
      <Right>
        <Text>{data}</Text>
        <Icon active name={rightIcon} />
      </Right>
    </ListItem>
  );
};

export default ItemList;
