import { compose } from 'redux';
import { connect } from 'react-redux';
import { withFirebase } from 'react-redux-firebase';
import { updateUserProfile } from '../../../actions/auth';
import InfoElem from '../InfoElem';

const mapStateToProps = state => {
  const { error, loading } = state.auth;
  return {
    error,
    loading,
    user: state.firebase.auth,
  };
};

const mapDispatchToProps = {
  updateUserProfile,
};

export default compose(
  withFirebase,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(InfoElem);
