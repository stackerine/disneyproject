import { compose } from 'redux';
import { connect } from 'react-redux';
import { updateUserProfile } from '../../../actions/auth';
import { firestoreConnect } from 'react-redux-firebase';
import { getUser } from '../../../services/auth';
import AccountPage from '../AccountPage';

const mapStateToProps = state => {
  return {
    user: state.firebase.auth,
  };
};

const mapDispatchToProps = {
  updateUserProfile,
};

export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(AccountPage);
