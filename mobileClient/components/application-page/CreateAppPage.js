import React, { Component } from 'react';
import { Switch, Button } from 'react-native';
import { withFirestore } from 'react-redux-firebase';
import { Item, Input, Spinner, Toast } from 'native-base';

import { createApplication } from '../../services/applications';

import {
  CreateAppContainer,
  NewAppInput,
  NewAppSwitch,
  SwitchText,
} from './style/createAppStyle';

class CreateAppPage extends Component {
  state = {
    title: '',
    description: '',
    isPublic: false,
    isLoading: false,
  };

  changeValue = (field, text) => {
    this.setState({ [field]: text });
  };

  changeSwitch = field => {
    this.setState(prevState => ({ [field]: !prevState[field] }));
  };

  saveApplication = async () => {
    const { title, description, isPublic } = this.state;

    this.setState({ isLoading: true });

    const { error, application } = await createApplication(
      {
        title,
        description,
        isPublic,
      },
      this.props.firestore
    );

    this.setState(
      {
        isLoading: false,
      },
      () => {
        if (error) {
          Toast.show({
            text: error,
            type: 'danger',
          });
        } else {
          /*TODO ADD CHANGE PAGE TO GO TO EDIT APP PAGE WITH appCreatedId AS PARAMS*/
        }
      }
    );
  };

  render() {
    const { title, description, isPublic, isLoading } = this.state;
    const loadingElt = isLoading && <Spinner />;

    return (
      loadingElt || (
        <CreateAppContainer>
          <NewAppInput>
            <Item rounded>
              <Input
                value={title}
                onChangeText={this.changeValue.bind(null, 'title')}
                placeholder="Application title"
              />
            </Item>
          </NewAppInput>
          <NewAppInput>
            <Item rounded>
              <Input
                value={description}
                onChangeText={this.changeValue.bind(null, 'description')}
                placeholder="Description"
              />
            </Item>
          </NewAppInput>

          <NewAppSwitch>
            <SwitchText>Public</SwitchText>
            <Switch
              value={isPublic}
              onChange={this.changeSwitch.bind(null, 'isPublic')}
            />
          </NewAppSwitch>

          <Button title="Next" onPress={this.saveApplication} />
        </CreateAppContainer>
      )
    );
  }
}

export default withFirestore(CreateAppPage);
