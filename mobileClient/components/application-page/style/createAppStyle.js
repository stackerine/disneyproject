import styled from 'styled-components';
import { View, Text, Switch, Button } from 'react-native';

export const CreateAppContainer = styled(View)`
  padding: 10px;
`;

export const NewAppInput = styled(View)`
  margin-bottom: 10px;
`;

export const NewAppSwitch = styled(View)`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 30px;
`;
export const SwitchText = styled(Text)`
  font-size: 17px;
`;
