import React from 'react';
import { View, FlatList } from 'react-native';
import { Text, Button, Icon } from 'native-base';

import {
  ModulesTypesContainer,
  ModuleTypeLine,
  ModuleTypeLineSeparator,
  ModuleTypeName,
} from './style/modulesTypesStyle';

export default function DrawerModulesTypes({ modulesTypes, addModule }) {
  return (
    <ModulesTypesContainer>
      <FlatList
        data={modulesTypes}
        keyExtractor={item => item.id}
        renderItem={({ item: { name, description, id } }) => (
          <ModuleTypeLine>
            <View>
              <ModuleTypeName>{name}</ModuleTypeName>
              <Text>{description}</Text>
            </View>
            <Button small rounded bordered onPress={addModule.bind(null, id)}>
              <Icon name="add" />
            </Button>
          </ModuleTypeLine>
        )}
        ItemSeparatorComponent={() => <ModuleTypeLineSeparator />}
      />
    </ModulesTypesContainer>
  );
}
