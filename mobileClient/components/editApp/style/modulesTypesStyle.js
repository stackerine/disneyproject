import styled from 'styled-components';
import { View } from 'react-native';
import { Text } from 'native-base';

export const ModulesTypesContainer = styled(View)`
  background-color: white;
  padding: 10px;
  height: 100%;
`;

export const ModuleTypeLine = styled(View)`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const ModuleTypeLineSeparator = styled(View)`
  background-color: silver;
  height: 1;
  margin: 10px 0;
`;

export const ModuleTypeName = styled(Text)`
  text-transform: capitalize;
  font-weight: bold;
`;
