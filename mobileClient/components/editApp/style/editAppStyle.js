import styled from 'styled-components';
import { View } from 'react-native';
import { Text, Container, Item } from 'native-base';

export const EditAppContainer = styled(Container)`
  padding: 5px;
`;

export const InfoTitle = styled(Text)`
  text-transform: capitalize;
  font-weight: bold;
`;

export const InfosItem = styled(Item)`
  margin-top: 5px;
  margin-bottom: 20px;
`;

export const SwitchView = styled(View)`
  flex-direction: row;
  align-items: baseline;
  justify-content: space-between;
  margin-bottom: 30px;
`;
