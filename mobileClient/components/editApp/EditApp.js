import { createStackNavigator } from 'react-navigation';

import EditAppInfos from './redux-connected/EditAppInfos';
import EditAppHome from './redux-connected/EditAppHome';
import EditAppModules from './redux-connected/EditAppModules';

export default createStackNavigator(
  {
    Home: EditAppHome,
    EditAppInfos: EditAppInfos,
    EditAppModules: EditAppModules,
  },
  {
    initialRouteName: 'Home',
    headerMode: 'none',
  }
);
