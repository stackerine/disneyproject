import React, { Component } from 'react';
import { withFirestore } from 'react-redux-firebase';
import { View } from 'react-native';
import {
  Text,
  Button,
  Switch,
  Icon,
  Item,
  Input,
  Textarea,
  Toast,
} from 'native-base';

import {
  fetchApplicationInfos,
  saveChangeInfos,
} from '../../services/applications';

import {
  EditAppContainer,
  InfoTitle,
  InfosItem,
  SwitchView,
} from './style/editAppStyle';

class EditAppInfos extends Component {
  state = { title: '', description: '', public: false };

  componentDidMount() {
    this.getApplicationInfos();
  }

  getApplicationInfos = async () => {
    const { apps } = this.props;
    const { title, description, public: isPublic } = apps[0];

    this.setState({ title, description, public: isPublic });
  };

  editInformation = (field, value) => {
    this.setState({ [field]: value });
  };

  changeSwitch = field => {
    this.setState(prevState => ({ [field]: !prevState[field] }));
  };

  saveChanges = async () => {
    const appId = this.props.navigation.getParam('appId');
    const { title, description, public: isPublic } = this.state;
    await saveChangeInfos({
      appId,
      data: { title, description, public: isPublic },
    });
    this.props.navigation.push('Home', {
      appId: appId,
    });
  };

  cancel = () => {
    this.props.navigation.navigate('Home');
  };

  render() {
    const { title, description, public: isPublic } = this.state;

    return (
      <EditAppContainer>
        <View>
          <InfoTitle>Title</InfoTitle>
          <InfosItem regular>
            <Input
              Textbox
              placeholder="Title"
              value={title}
              onChangeText={this.editInformation.bind(null, 'title')}
            />
          </InfosItem>
        </View>
        <View>
          <InfoTitle>Description</InfoTitle>
          <InfosItem regular>
            <Textarea
              rowSpan={5}
              placeholder="Description"
              value={description}
              onChangeText={this.editInformation.bind(null, 'description')}
            />
          </InfosItem>
        </View>
        <SwitchView>
          <InfoTitle>Public</InfoTitle>
          <Switch
            value={isPublic}
            onChange={this.changeSwitch.bind(null, 'public')}
          />
        </SwitchView>
        <View>
          <Button iconLeft block success onPress={this.saveChanges}>
            <Icon name="md-checkmark" />
            <Text>Save</Text>
          </Button>
          <Button
            iconLeft
            block
            danger
            bordered
            onPress={this.cancel}
            style={{ marginTop: 5 }}
          >
            <Icon name="md-close" />
            <Text>Cancel</Text>
          </Button>
        </View>
      </EditAppContainer>
    );
  }
}

export default withFirestore(EditAppInfos);
