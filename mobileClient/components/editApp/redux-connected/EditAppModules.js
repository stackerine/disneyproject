import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';

import EditAppModules from '../EditAppModules';

const mapStateToProps = state => {
  return {
    modulesTypes: state.firestore.ordered.modulesTypes,
    modules: state.firestore.ordered.modules,
  };
};

const mapDispatchToProps = {};

export default compose(
  firestoreConnect(props => {
    const appId = props.navigation.getParam('appId');
    return [
      { collection: 'modulesTypes' },
      {
        collection: 'apps',
        doc: appId,
        subcollections: [{ collection: 'modules' }],
        storeAs: 'modules',
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(EditAppModules);
