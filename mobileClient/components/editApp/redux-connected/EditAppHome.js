import { compose } from 'redux';
import { connect } from 'react-redux';
import { getUser } from '../../../services/auth';
import { firestoreConnect } from 'react-redux-firebase';

import EditAppHome from '../EditAppHome';

const mapStateToProps = state => {
  return {
    apps: state.firestore.ordered.apps,
    modules: state.firestore.ordered.modules,
  };
};

const mapDispatchToProps = {};

export default compose(
  firestoreConnect(props => {
    const appId = props.navigation.getParam('appId');
    return [
      {
        collection: 'apps',
        doc: appId,
      },
      {
        collection: 'apps',
        doc: appId,
        subcollections: [{ collection: 'modules' }],
        storeAs: 'modules',
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(EditAppHome);
