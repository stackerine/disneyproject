import { compose } from 'redux';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';

import EditAppInfos from '../EditAppInfos';

const mapStateToProps = state => {
  return {
    apps: state.firestore.ordered.apps,
  };
};

const mapDispatchToProps = {};

export default compose(
  firestoreConnect(props => {
    const appId = props.navigation.getParam('appId');
    return [
      {
        collection: 'apps',
        doc: appId,
      },
    ];
  }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
)(EditAppInfos);
