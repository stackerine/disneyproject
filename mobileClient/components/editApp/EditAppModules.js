import React, { Component } from 'react';

import { withFirestore } from 'react-redux-firebase';
import { ScrollView, View, DrawerLayoutAndroid, FlatList } from 'react-native';
import { Text, Button, Icon, Toast, Drawer } from 'native-base';

import { saveChangeModules } from '../../services/modules';

import { EditAppContainer } from './style/editAppStyle';

import { ModuleTypeLineSeparator } from './style/modulesTypesStyle';

import DrawerModulesTypes from './DrawerModulesTypes';

class EditAppModules extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modules: [],
    };
  }
  componentDidMount() {
    this.getApplicationsModules();
  }

  getApplicationsModules = () => {
    const { modules } = this.props;

    this.setState({ modules });
  };

  openDrawer = () => {
    this.drawer._root.open();
  };

  saveChanges = async () => {
    const { modules } = this.state;

    const {
      navigation: { getParam },
    } = this.props;
    const appId = getParam('appId');

    const { firestore } = this.props;
    const { error } = await saveChangeModules({ appId, modules }, firestore);

    if (!error) {
      this.props.navigation.push('Home', {
        appId: appId,
      });
    } else {
      Toast.show({
        text: error,
        type: 'danger',
      });
    }
  };

  cancel = () => {
    this.props.navigation.navigate('Home');
  };

  addModule = async typeId => {
    const { modulesTypes } = this.props;
    this.setState(prevState => {
      const moduleType = modulesTypes.find(({ id }) => id === typeId);

      return {
        modules: [
          ...prevState.modules,
          {
            title: `New ${moduleType.name} module`,
            type: typeId,
            content: moduleType.contentFormat,
          },
        ],
      };
    });
    this.drawer._root.close();
  };

  render() {
    const { modules } = this.state;
    const { modulesTypes } = this.props;

    const showModuleList = (
      <FlatList
        data={modules}
        keyExtractor={(item, index) => {
          return item.id || index.toString();
        }}
        renderItem={({ item: moduleInfos }) => (
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Text>{moduleInfos.title}</Text>
            <View style={{ flexDirection: 'row' }}>
              <Button small>
                <Icon name="md-create" />
              </Button>
              <Button small bordered>
                <Icon name="md-move" />
              </Button>
            </View>
          </View>
        )}
        ItemSeparatorComponent={() => <ModuleTypeLineSeparator />}
      />
    );
    return (
      <Drawer
        content={
          <DrawerModulesTypes
            modulesTypes={modulesTypes}
            addModule={this.addModule}
          />
        }
        ref={_drawer => (this.drawer = _drawer)}
      >
        {
          <EditAppContainer>
            <View>
              <Button iconLeft block success onPress={this.saveChanges}>
                <Icon name="md-checkmark" />
                <Text>Save</Text>
              </Button>
              <Button
                iconLeft
                block
                danger
                bordered
                onPress={this.cancel}
                style={{ marginTop: 5 }}
              >
                <Icon name="md-close" />
                <Text>Cancel</Text>
              </Button>
            </View>
            <ScrollView style={{ marginTop: 10, marginBottom: 10 }}>
              {showModuleList}
            </ScrollView>
            <Button block iconLeft bordered dark onPress={this.openDrawer}>
              <Icon name="md-add" />
              <Text>Add module</Text>
            </Button>
          </EditAppContainer>
        }
      </Drawer>
    );
  }
}

export default withFirestore(EditAppModules);
