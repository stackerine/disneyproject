import React, { Component } from 'react';
import { withFirestore } from 'react-redux-firebase';

import { View } from 'react-native';

import {
  Spinner,
  Text,
  Button,
  Icon,
  H2,
  Card,
  CardItem,
  Body,
  Toast,
} from 'native-base';

import { EditAppContainer } from './style/editAppStyle';

class EditAppHome extends Component {
  constructor(props) {
    super(props);
  }

  static navigationOptions = ({ navigation }) => {
    return {
      title: `Editing ${navigation.getParam('title')}`,
    };
  };

  editAppInfos = appId => {
    this.props.navigation.navigate('EditAppInfos', {
      appId,
    });
  };

  editModules = appId => {
    this.props.navigation.navigate('EditAppModules', {
      appId,
    });
  };

  render() {
    const { modules = [], apps } = this.props;
    const {
      id,
      title,
      description,
      public: isPublic,
      orderModules = [],
    } = apps[0];

    const showModules = modules.map((moduleInfos, index) => (
      <Text key={moduleInfos.id}>{moduleInfos.title}</Text>
    ));

    return (
      (
        <EditAppContainer>
          <Card>
            <CardItem header>
              <Text>Application Informations</Text>
            </CardItem>
            <CardItem>
              <Body>
                <H2 style={{ marginBottom: 10 }}>{title}</H2>
                <Text style={{ marginBottom: 10, fontStyle: 'italic' }}>
                  {description}
                </Text>
                <View style={{ flexDirection: 'row' }}>
                  <Icon
                    style={{ fontSize: 20, marginRight: 5 }}
                    name={isPublic ? 'md-contacts' : 'md-lock'}
                  />
                  <Text>{isPublic ? 'Public' : 'Private'}</Text>
                </View>
              </Body>
            </CardItem>
            <CardItem footer>
              <Button
                iconLeft
                rounded
                bordered
                dark
                small
                onPress={this.editAppInfos.bind(null, id)}
              >
                <Icon active name="md-create" />
                <Text>Edit</Text>
              </Button>
            </CardItem>
          </Card>

          <Card>
            <CardItem header>
              <Text>Modules</Text>
            </CardItem>
            <CardItem>
              {
                <Body>
                  {(modules.length && orderModules.length && showModules) || (
                    <Text>No module yet</Text>
                  )}
                </Body>
              }
            </CardItem>
            <CardItem footer>
              <Button
                iconLeft
                rounded
                bordered
                dark
                small
                onPress={this.editModules.bind(null, id)}
              >
                <Icon name="md-square-outline" />
                <Text>Edit modules</Text>
              </Button>
            </CardItem>
          </Card>
        </EditAppContainer>
      ) || (
        <EditAppContainer>
          <Spinner />
        </EditAppContainer>
      )
    );
  }
}

export default withFirestore(EditAppHome);
