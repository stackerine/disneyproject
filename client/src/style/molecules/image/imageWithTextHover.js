import React from 'react';
import styled from 'styled-components';

const ImageWithTextHover = ({ img, children }) => {
  return (
    <SubContainer>
      <Image img={img} />
      <Text>
        <p>{children}</p>
      </Text>
    </SubContainer>
  );
};

const Text = styled.div`
  background-color: rgba(0, 0, 0, 0.7);
  color: white;
  position: absolute;
  max-height: 100px;
  bottom: -100px;
  animation: 0.3s;
  padding: 10px;
  text-align: justify;
`;

const Image = styled.div`
  width: 100%;
  height: 100%;
  background-image: url(${({ img }) => `${img}`});
  background-position: center;
  background-size: cover;
  position: absolute;
`;

const SubContainer = styled.div`
  width: 30%;
  height: 300px;
  border: solid 1px black;
  position: relative;
  overflow: hidden;
  cursor: pointer;
  animation: 0.3s;
  :hover ${Text} {
    transform: translateY(-100px);
  }
  :hover ${Image} {
    transform: scale(2);
  }
`;

export default ImageWithTextHover;
