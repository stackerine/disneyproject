import React from 'react';
import ImgContainer from '../../atomes/image/imgContainer';

const ImageContainer = ({ imgLink, imgAlt }) => {
  return (
    <ImgContainer>
      <img src={imgLink} alt={imgAlt} />
    </ImgContainer>
  );
};

export default ImageContainer;
