import React from 'react';
import ImageContainer from '../../molecules/image/squareImg';
import ContainerWithShadow from '../../atomes/container/containerWithShadow';

const ImageWithText = ({ imgLink, imgAlt, children }) => {
  return (
    <ContainerWithShadow width={300}>
      <ImageContainer imgLink={imgLink} imgAlt={imgAlt} />
      <p>{children}</p>
    </ContainerWithShadow>
  );
};

export default ImageWithText;
