import React from 'react';
import ActionButton from '../../atomes/button/index';

const SignIn = ({ children }) => {
  return (
    <ActionButton variant="outlined" color="secondary">
      {children}
    </ActionButton>
  );
};

export default SignIn;
