import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

class SimpleMenu extends Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({
      anchorEl: event.currentTarget,
    });
  };

  handleClose = (url, e) => {
    this.setState(
      {
        anchorEl: null,
      },
      () => {
        if (url) {
          this.props.history.push(url);
        }
      }
    );
  };

  render() {
    const { anchorEl } = this.state;
    const { buttonContent, menuLinks } = this.props;
    return (
      <div>
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          {buttonContent}
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={this.handleClose.bind(this, null)}
        >
          {menuLinks.map(({ title, url }) => (
            <MenuItem key={title} onClick={this.handleClose.bind(this, url)}>
              {title}
            </MenuItem>
          ))}
        </Menu>
      </div>
    );
  }
}

export default withRouter(SimpleMenu);
