import React from 'react';

import HeaderContainer from '../../atomes/header/HeaderContainer';
import HeaderPart from '../../atomes/header/HeaderParts';


const Header = ({ leftPart, rightPart }) => (
  <HeaderContainer>
    <HeaderPart>{leftPart}</HeaderPart>
    <HeaderPart>{rightPart}</HeaderPart>
  </HeaderContainer>
);

export default Header;
