import styled from 'styled-components';

const ContainerWithShadow = styled.div`
  width: ${({ width }) => width}px;
  box-shadow: 0px 0px 5px 2px rgba(0, 0, 0, 0.2);
  padding: 20px;
  text-align: justify;
  span {
    text-align: center;
  }
`;

export default ContainerWithShadow;
