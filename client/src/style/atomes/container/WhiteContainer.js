import styled from 'styled-components';

export default styled.div`
  background-color: white;
  padding: 20px;
  margin: 10px;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  border: #e8e8e8 1px solid;
  :hover {
    box-shadow: black 5px 5px 0px 0px;
    transition: all 0.3s ease;
  }
`;
