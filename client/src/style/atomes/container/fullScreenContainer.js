import styled from 'styled-components';

const FullScreenContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100vh;
  background-color: ${({ color }) => color};
`;

export default FullScreenContainer;
