import styled from 'styled-components';

export default styled.div`
  width: 100%;
  height: 25vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${({ backgroundColor }) => backgroundColor || 'white'};
  background-image: ${({ backgroundImage }) =>
    `url(${backgroundImage})` || 'none'};
  background-size: cover;
`;
