import React from 'react';
import styled from 'styled-components';

const Component = styled.div`
  position: relative;
  width: ${({ width }) => width || 'auto'};
  height: ${({ height }) => height || 'auto'};
`;

const ImageBG = styled.div`
  width: 100%;
  height: 100%;
  background-image: url(${({ image }) => image});
  background-position: center center;
  background-size: cover;
  position: absolute;
  z-index: -2;
`;

const GradientBG = styled.div`
  width: 100%;
  height: 100%;
  background: ${(({ gradient }) => gradient) || 'transparent'};
  position: absolute;
  opacity: ${({ gradientOpacity }) => gradientOpacity};
  z-index: -1;
`;

const ChildrenContainer = styled.div`
  z-index: 10;
  height: 100%;
`;

export default ({
  height,
  image,
  gradient,
  gradientOpacity = '0.9',
  children,
}) => {
  return (
    <Component height={height}>
      <ImageBG image={image} />
      <GradientBG gradient={gradient} gradientOpacity={gradientOpacity} />
      <ChildrenContainer>{children}</ChildrenContainer>
    </Component>
  );
};
