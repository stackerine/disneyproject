import React from 'react';
import styled from 'styled-components';

const ContainerWithText = ({ children }) => <Container>{children}</Container>;

export default ContainerWithText;

const Container = styled.div`
  position: relative;
  z-index: 10;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: white;
`;
