import styled from 'styled-components';

const Banner = styled.div`
  width: 100%;
  padding-top: 5%;
  padding-bottom: 5%;
  background-color: ${({ color }) => color};
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

export default Banner;
