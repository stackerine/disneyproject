import styled from 'styled-components';
import Input from '@material-ui/core/Input';

export default styled(Input)`
  margin-bottom: 10px;
`;
