import React from 'react';

import styled from 'styled-components';
import { Link } from 'react-router-dom';

import Button from '../button/comicStyleButton';

const LinkWoutUnderline = styled(Link)`
  text-decoration: none;
  margin: ${({ margin }) => margin};
`;

export default ({ linkDir = '/', title = '', margin = 0, ...otherProps }) => (
  <LinkWoutUnderline to={linkDir} margin={margin}>
    <Button {...otherProps}>{title}</Button>
  </LinkWoutUnderline>
);
