import styled from 'styled-components';

export default styled.button`
  border: #000 3px solid;
  background-color: white;
  color: black;
  box-shadow: #000000 5px 5px 0px;
  text-transform: uppercase;
  padding: 6px 20px;
  font-size: 1.15rem;
  font-weight: 700;
  border-radius: 3px;
  cursor: pointer;
  font-family: 'Patrick Hand', cursive;
  transition: all 0.1s ease-in-out;
  :hover {
    box-shadow: #000000 7px 7px 0px;
    transition: all 0.1s ease-in-out;
  }
`;
