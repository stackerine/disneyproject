import React from 'react';
import Button from '@material-ui/core/Button';

const ActionButton = ({ children, variant, color }) => {
  return (
    <Button variant={variant} color={color}>
      {children}
    </Button>
  );
};

export default ActionButton;
