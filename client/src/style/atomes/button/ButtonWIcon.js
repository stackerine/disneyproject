import React from 'react';
import styled from 'styled-components';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';

const StyledButton = styled(Button)`
  margin: 5px;
`;

const StyledIcon = styled(Icon)`
  margin-right: 5px;
`;

const ButtonWIcon = ({ icon, text, ...otherProps }) => {
  return (
    <StyledButton {...otherProps}>
      <StyledIcon className={icon} />
      {text}
    </StyledButton>
  );
};

export default ButtonWIcon;
