import styled from 'styled-components';
import Link from '@material-ui/core/Link';

const HeaderLink = styled(Link)`
  padding: 0 20px;
`;

export default HeaderLink;
