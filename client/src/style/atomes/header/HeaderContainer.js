import styled from 'styled-components';
import { PALE_WHITE } from '../../constantes';

const HeaderContainer = styled.div`
  background-color: ${PALE_WHITE};
  width: 100%;
  height: 100px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export default HeaderContainer;
