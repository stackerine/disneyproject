import styled from 'styled-components';

const HeaderPart = styled.div`
  margin: 0 20px;
`;

export default HeaderPart;
