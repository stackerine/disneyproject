import styled from 'styled-components';
import { PALE_RED } from '../../constantes';

const Logo = styled.div`
  background-color: ${PALE_RED};
  border-radius: 50%;
  height: 70px;
  width: 70px;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  :hover {
    cursor: pointer;
  }
`;

export default Logo;
