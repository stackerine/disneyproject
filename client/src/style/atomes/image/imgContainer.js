import styled from 'styled-components';

const ImgContainer = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  img {
    width: 100%;
  }
`;

export default ImgContainer;
