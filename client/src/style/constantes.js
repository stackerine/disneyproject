export const PALE_GREEN = '#2ecc71';
export const PALE_RED = '#e74c3c';
export const PALE_WHITE = '#ecf0f1';
export const TEAL = '#b2dfdb';
export const PALE_GRAY = '#b1b1b1';
export const BLACK_OPACITY30 = '#00000044';
export const PALE_BLACK = '#282828';
