import React, { Component } from 'react';
import { Field } from './style/accountPageStyle';
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import Visibility from '@material-ui/icons/Visibility';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

const toggleIsShownPassword = ({ isShown }) => ({
  isShown: !isShown,
});

export default class PasswordField extends Component {
  state = {
    isShown: false,
  };

  updateFieldContent = ({ target: { value } }) => {
    const { changeFieldContent, fieldName } = this.props;
    changeFieldContent(fieldName, value);
  };

  onClickIconButton = () => {
    this.setState(toggleIsShownPassword);
  };

  render() {
    const { title, value } = this.props;
    const { isShown } = this.state;
    const createEndAdornment = (
      <InputAdornment position="end">
        <IconButton
          aria-label="Toggle password visibility"
          onClick={this.onClickIconButton}
        >
          {isShown ? <Visibility /> : <VisibilityOff />}
        </IconButton>
      </InputAdornment>
    );

    return (
      <Field>
        <FormControl>
          <InputLabel>{title}</InputLabel>
          <Input
            type={isShown ? 'text' : 'password'}
            onChange={this.updateFieldContent}
            value={value}
            endAdornment={createEndAdornment}
          />
        </FormControl>
      </Field>
    );
  }
}
