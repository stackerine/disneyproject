import React from 'react';
import { SubContainer, Row } from './style/accountPageStyle';
import TextField from '@material-ui/core/TextField';

const FieldComponent = ({
  onChangeFieldInput,
  title,
  fieldName,
  data,
  placeholder,
  error,
  value,
  onClickRemoveInfoFromServer,
  name,
}) => {
  const displayedFieldData = data && (
    <div>
      <span>
        {data}{' '}
        <i
          className="fas fa-times"
          onClick={onClickRemoveInfoFromServer.bind(null, name)}
        />
      </span>
    </div>
  );
  return (
    <SubContainer>
      <Row>
        <div>{title}</div>
        {displayedFieldData}
        <div>
          <TextField
            type="text"
            placeholder={placeholder}
            onChange={onChangeFieldInput.bind(null, fieldName)}
            label={error || placeholder}
            value={value}
          />
        </div>
      </Row>
    </SubContainer>
  );
};

export default FieldComponent;
