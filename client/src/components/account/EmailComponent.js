import React, { Component } from 'react';
import { FieldUpdate, Field, FeedbackMessage } from './style/accountPageStyle';
import TextField from '@material-ui/core/TextField';
import { isValidEmail } from '../../../../commons/formVerification';
import { updateUserEmail } from '../../services/users';
import ComicButton from '../../style/atomes/button/comicStyleButton';

export default class EmailComponent extends Component {
  state = {
    currentEmail: '',
    newEmail: '',
    errors: {},
  };

  onChangeFieldInput = (fieldName, { target: { value } }) => {
    this.setState({ [fieldName]: value });
  };

  onClickSaveEmail = async () => {
    const { currentEmail, newEmail } = this.state;
    if (this.checkEmailValidity()) {
      const errors = await updateUserEmail(
        currentEmail.toLowerCase(),
        newEmail.toLowerCase()
      );
      this.setState({ errors, currentEmail: '', newEmail: '' });
    }
  };

  checkEmailValidity = () => {
    const { currentEmail, newEmail } = this.state;
    const { email } = this.props;
    const errCurrentEmail =
      currentEmail !== email && "That's not your current email.";
    const errNewEmail =
      !isValidEmail(newEmail) && 'Please, enter a valid email.';
    const errors = { errCurrentEmail, errNewEmail };
    this.setState({ errors });
    return !errCurrentEmail && !errNewEmail;
  };

  render() {
    const {
      errors,
      errors: { errCurrentEmail, errNewEmail },
      currentEmail,
      newEmail,
    } = this.state;
    const message = (
      <FeedbackMessage isError={errors.isError}>
        {errors.message}
      </FeedbackMessage>
    );
    return (
      <FieldUpdate>
        <Field>
          <TextField
            type="text"
            placeholder={'Current email'}
            onChange={this.onChangeFieldInput.bind(this, 'currentEmail')}
            label={errCurrentEmail || 'Current email'}
            value={currentEmail}
          />
        </Field>
        <Field>
          <TextField
            type="text"
            placeholder={'New email'}
            onChange={this.onChangeFieldInput.bind(this, 'newEmail')}
            label={errNewEmail || 'New email'}
            value={newEmail}
          />
        </Field>
        {message}
        <ComicButton onClick={this.onClickSaveEmail}>Save email</ComicButton>
      </FieldUpdate>
    );
  }
}
