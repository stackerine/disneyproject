import React from 'react';
import { SubContainer, Row } from './style/accountPageStyle';
import ComicButton from '../../style/atomes/button/comicStyleButton';

const ContainerFieldComponent = ({
  children,
  title,
  isOpen,
  onClickUpdateInfo,
  value,
  elemToUpdate,
}) => {
  return (
    <SubContainer>
      <Row>
        <div>{title}</div>
        <div>{value}</div>
        <div>
          <ComicButton onClick={onClickUpdateInfo.bind(null, elemToUpdate)}>
            Change
          </ComicButton>
        </div>
      </Row>
      <div>{isOpen && children}</div>
    </SubContainer>
  );
};

export default ContainerFieldComponent;
