import React, { Component } from 'react';
import PasswordField from './PasswordField';
import { FieldUpdate, FeedbackMessage } from './style/accountPageStyle';
import { updateUserPassword } from '../../services/users';
import {
  isValidPassword,
  isDifferentValue,
} from '../../../../commons/formVerification';
import ComicButton from '../../style/atomes/button/comicStyleButton';
import md5 from 'md5';

class PasswordComponent extends Component {
  state = {
    currentPassword: '',
    newPassword1: '',
    newPassword2: '',
    errors: {},
  };

  changeFieldContent = (field, content) => {
    this.setState({ [field]: content });
  };

  validPasswordUpdate = async () => {
    const { currentPassword, newPassword2 } = this.state;
    if (this.checkPasswordValidity()) {
      const { isError, message } = await updateUserPassword(
        md5(currentPassword),
        md5(newPassword2)
      );
      this.setState({
        errors: {
          isError,
          message,
        },
        currentPassword: '',
        newPassword1: '',
        newPassword2: '',
      });
    }
  };

  checkPasswordValidity = () => {
    const { currentPassword, newPassword1, newPassword2 } = this.state;

    const errIdenticalPassword =
      !isDifferentValue(currentPassword, newPassword2) &&
      'You must choose a different password.';
    const errSamePassword =
      isDifferentValue(newPassword1, newPassword2) &&
      "You didn't enter the same password.";

    const errValidPassword =
      !isValidPassword(newPassword2) &&
      'Please enter a valid password. It can contain letters, numbers & symbols between 4 and 8 characters.';
    this.setState({
      errors: {
        isError: true,
        message: errSamePassword || errValidPassword || errIdenticalPassword,
      },
    });
    return !errIdenticalPassword && !errSamePassword && !errValidPassword;
  };

  render() {
    const { errors, currentPassword, newPassword1, newPassword2 } = this.state;
    const message = (
      <FeedbackMessage isError={errors.isError}>
        {errors.message}
      </FeedbackMessage>
    );

    return (
      <FieldUpdate>
        <PasswordField
          fieldName="currentPassword"
          title="Current Password:"
          changeFieldContent={this.changeFieldContent}
          value={currentPassword}
        />
        <PasswordField
          fieldName="newPassword1"
          title="New Password:"
          changeFieldContent={this.changeFieldContent}
          value={newPassword1}
        />
        <PasswordField
          fieldName="newPassword2"
          title="New Password:"
          changeFieldContent={this.changeFieldContent}
          value={newPassword2}
        />
        {message}
        <ComicButton onClick={this.validPasswordUpdate}>
          Save Password
        </ComicButton>
      </FieldUpdate>
    );
  }
}

export default PasswordComponent;
