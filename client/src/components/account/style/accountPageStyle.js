import styled from 'styled-components';
import { PALE_WHITE, PALE_RED, PALE_GREEN } from '../../../style/constantes';

export const Title = styled.h1`
  margin-bottom: 2%;
  font-size: 6rem;
  text-transform: uppercase;
  font-family: 'Bangers', cursive;
  letter-spacing: 0.05em;
  text-shadow: -3px -3px 0 #000, 3px -3px 0 #000, -3px 3px 0 #000,
    3px 3px 0 #000, 7px 10px 0px #2a4452, -9px -8px 0px #fff195;
  color: #e63b3b;
`;

export const Container = styled.div`
  text-align: center;
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const ContainerAccount = styled.div`
  align-items: center;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const SubContainer = styled.div`
  width: 80%;
  border-top: solid 1px ${PALE_WHITE};
  padding: 10px;
  margin: 0 auto;
  i {
    cursor: pointer;
  }
`;

export const Row = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  padding: 10px;
`;

export const FieldUpdate = styled.div`
  background-color: ${PALE_WHITE};
  padding: 10px;
  text-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 20px;
`;

export const Field = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px;
`;

export const FeedbackMessage = styled.div`
  color: ${({ isError }) => (isError ? PALE_RED : PALE_GREEN)};
  padding: 10px;
`;
