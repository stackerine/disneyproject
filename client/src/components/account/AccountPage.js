import React, { Component } from 'react';
import withAuth from '../../hoc/withAuth';
import { getUser, updateUserInfo, removeUserInfo } from '../../services/users';
import { Container, Title, ContainerAccount } from './style/accountPageStyle';
import PasswordComponent from './PasswordComponent';
import FieldComponent from './FieldComponent';
import {
  isNotEmpty,
  isValidUsername,
  isValidPhoneNumber,
} from '../../../../commons/formVerification';
import EmailComponent from './EmailComponent';
import Header from '../../style/molecules/header/Header';
import ContainerFieldComponent from './ContainerFieldComponent';
import ComicButton from '../../style/atomes/button/comicStyleButton';
import _ from 'lodash';

const updateInfo = newUserInfo => ({ userInfo }) => ({
  userInfo: { ...userInfo, ...newUserInfo },
  usernameField: '',
  phoneNumberField: '',
});

const toggleFieldIsShown = field => prevState => ({
  [field]: !prevState[field],
});

const EMAIL_UPDATE = 'isEmailUpdate';
const PSWD_UPDATE = 'isPasswordUpdate';

class AccountPage extends Component {
  state = {
    userInfo: {},
    isPasswordUpdate: false,
    isEmailUpdate: false,
    usernameField: '',
    emailField: '',
    phoneNumberField: '',
    errors: {},
    newUserInfo: {},
  };

  componentDidMount = () => {
    this.fetchUserInfo();
  };

  fetchUserInfo = async () => {
    this.setState({ userInfo: await getUser() });
  };

  onClickSaveChanges = async () => {
    const { usernameField, phoneNumberField } = this.state;
    const newUsername = isNotEmpty(usernameField) && usernameField;
    const newPhoneNumber = isNotEmpty(phoneNumberField) && phoneNumberField;
    if (this.checkInputValidation()) {
      const {
        userInfo: { username, phoneNumber },
      } = await updateUserInfo({ newUsername, newPhoneNumber });
      this.setState(updateInfo({ username, phoneNumber }));
    }
  };

  checkInputValidation = () => {
    const { usernameField, phoneNumberField } = this.state;

    const errUsername =
      isNotEmpty(usernameField) &&
      !isValidUsername(usernameField) &&
      'Please enter a different & valid username.';
    const errPhoneNumber =
      isNotEmpty(phoneNumberField) &&
      !isValidPhoneNumber(phoneNumberField) &&
      'Please enter a valid phone number.';

    const errors = {
      errUsername,
      errPhoneNumber,
    };

    this.setState({
      errors,
    });

    return !errUsername && !errPhoneNumber;
  };

  onClickUpdateInfo = field => {
    this.setState(toggleFieldIsShown(field));
  };

  onChangeFieldInput = (fieldName, { target: { value } }) => {
    this.setState({ [fieldName]: value });
  };

  onClickRemoveInfoFromServer = async fieldName => {
    const { updatedInfo } = await removeUserInfo(fieldName);
    const inputFieldName =
      fieldName === 'phone_number' ? 'phoneNumber' : _.camelCase(fieldName);
    this.setState(updateInfo({ [inputFieldName]: updatedInfo }));
  };

  render() {
    const {
      userInfo: { username, email, phoneNumber },
      isPasswordUpdate,
      isEmailUpdate,
      errors: { errUsername, errPhoneNumber },
      usernameField,
      phoneNumberField,
    } = this.state;
    const defaultEmail =
      (email &&
        `${email
          .split('')
          .slice(0, 3)
          .join('')} * * * * *`) ||
      '* * * * *';
    return (
      <Container>
        <Header />
        <ContainerAccount>
          <Title>My Account</Title>
          <FieldComponent
            onChangeFieldInput={this.onChangeFieldInput}
            title={'Username: '}
            fieldName={'usernameField'}
            data={username}
            placeholder={'New username'}
            error={errUsername}
            value={usernameField}
            onClickRemoveInfoFromServer={this.onClickRemoveInfoFromServer}
            name={'username'}
          />
          <ContainerFieldComponent
            title={'Password: '}
            value={'* * * * * *'}
            isOpen={isPasswordUpdate}
            onClickUpdateInfo={this.onClickUpdateInfo}
            elemToUpdate={PSWD_UPDATE}
          >
            <PasswordComponent />
          </ContainerFieldComponent>
          <ContainerFieldComponent
            title={'Email: '}
            value={defaultEmail}
            isOpen={isEmailUpdate}
            onClickUpdateInfo={this.onClickUpdateInfo}
            elemToUpdate={EMAIL_UPDATE}
          >
            <EmailComponent email={email} />
          </ContainerFieldComponent>
          <FieldComponent
            onChangeFieldInput={this.onChangeFieldInput}
            title={'Phone number: '}
            fieldName={'phoneNumberField'}
            data={phoneNumber}
            placeholder={'New phone number.'}
            error={errPhoneNumber}
            value={phoneNumberField}
            onClickRemoveInfoFromServer={this.onClickRemoveInfoFromServer}
            name={'phone_number'}
          />
          <ComicButton onClick={this.onClickSaveChanges}>
            Save changes
          </ComicButton>
        </ContainerAccount>
      </Container>
    );
  }
}

export default withAuth(AccountPage);
