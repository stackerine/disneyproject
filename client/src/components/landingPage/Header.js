import React, { Fragment } from 'react';
import LandingPage from './Home';
import { Link } from 'react-router-dom';
import Logo from '../../style/atomes/logo';
import HeaderLink from '../../style/atomes/header/HeaderLink';
import SignInButton from '../../style/molecules/button/signIn';
import HeaderContainer from '../../style/molecules/header/index';

const leftHeaderPart = (
  <Fragment>
    <HeaderLink component={LandingPage} to="/home">
      <Logo>Logo</Logo>
    </HeaderLink>
  </Fragment>
);

const rightHeaderPart = (
  <Fragment>
    <Link to="./login">
      <SignInButton>Sign in</SignInButton>
    </Link>
  </Fragment>
);

const Header = () => {
  return (
    <HeaderContainer leftPart={leftHeaderPart} rightPart={rightHeaderPart} />
  );
};

export default Header;
