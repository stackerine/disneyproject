import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  text-align: center;
`;

export const Title = styled.h1`
  margin-top: 0;
  margin-bottom: 150px;
  font-size: 8rem;
  text-transform: uppercase;
  font-family: 'Bangers', cursive;
  letter-spacing: 0.05em;
  text-shadow: -3px -3px 0 #000, 3px -3px 0 #000, -3px 3px 0 #000,
    3px 3px 0 #000, 7px 10px 0px #2a4452, -9px -8px 0px #fff195;
  color: #e63b3b;
`;

export const TextContainer = styled.div`
  font-size: 2em;
  color: white;
  margin-bottom: 50px;
  p:first-child {
    font-weight: bold;
  }
  p:last-child {
    font-weight: lighter;
  }
`;
