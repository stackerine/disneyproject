import React from 'react';
import { Container, Title, TextContainer } from './style/getStartedStyled.js';
import ContainerWithImageAndGradient from '../../style/atomes/container/containerWithImageAndGradient';
import LinkButton from '../../style/atomes/button/LinkButton';

const Home = () => {
  return (
    <ContainerWithImageAndGradient
      height="100vh"
      image={'/assets/images/sun_background_circles.jpg'}
      gradient={
        'linear-gradient(to right bottom, #f06e7d, #ff8c6f, #ffb065, #ffd665, #fffd78)'
      }
      gradientOpacity="0.85"
    >
      <Container>
        <div>
          <Title>Baaap !</Title>
          {/* TODO CHANGE TITLE BY REAL APP LOGO */}
        </div>
        <TextContainer>
          <p>Une application pour créer vos propres applications.</p>
          <p>Tout simplement.</p>
        </TextContainer>
        <div>
          <LinkButton
            title="Discover"
            linkDir="./" /*TODO CREATE DISCOVER PAGE AND ADD LINK*/
            variant="contained"
            color="primary"
            size="large"
            margin="0 10px 0 0"
          />
          <LinkButton
            title="Sign in"
            linkDir="./login"
            variant="contained"
            color="primary"
            size="large"
            margin="0 0 0 10px"
          />
        </div>
      </Container>
    </ContainerWithImageAndGradient>
  );
};

export default Home;
