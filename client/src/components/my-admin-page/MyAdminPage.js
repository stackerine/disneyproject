import React, { Component } from 'react';

import withAuth from '../../hoc/withAuth';

import Header from '../../style/molecules/header/Header';

import { fetchUserApplication } from '../../services/applicationsCalls';

import WhiteContainer from '../../style/atomes/container/WhiteContainer';
import LinkWoutUnderline from '../../style/atomes/link/linkWoutUndeline';
import ContainerWithImageAndGradient from '../../style/atomes/container/containerWithImageAndGradient';

import NewAppButton from './style/newAppButton';
import {
  AppsContainer,
  AppBlock,
  AppTitle,
  AppLogo,
  NewAppContainer,
  NewAppButtonContainer,
} from './style/adminPageStyle';

class MyAdminPage extends Component {
  state = {
    applications: [],
    isAppLoading: true,
  };
  componentDidMount() {
    this.getUserApplication();
  }

  getUserApplication = async () => {
    const applications = await fetchUserApplication(
      1
    ); /*TODO: CHANGE WITH USER_ID*/
    this.setState({ applications, isAppLoading: false });
  };

  render() {
    const { applications } = this.state;

    const createNewApp = (
      <NewAppContainer>
        <ContainerWithImageAndGradient
          image={'/assets/images/sun_background.jpg'}
          gradientOpacity="0.8"
          height="25vh"
          gradient="linear-gradient(135deg, rgba(255,75,75,1) 0%, rgba(255,127,0,1) 100%)"
        >
          <NewAppButtonContainer>
            <NewAppButton
              linkDir="./application/new"
              title="Create new application"
            />
          </NewAppButtonContainer>
        </ContainerWithImageAndGradient>
      </NewAppContainer>
    );

    const existingApps = (
      <AppsContainer>
        {applications.map(({ id, title }) => (
          <AppBlock key={id}>
            <LinkWoutUnderline to={`./application/${id}`}>
              <WhiteContainer>
                <AppTitle>{title}</AppTitle>
                <AppLogo src="/assets/images/tricy_application_icon.png" />
                {/*TODO SAVE LOGO FOR APP AND REPLACE IMAGE LINK WITH IT*/}
              </WhiteContainer>
            </LinkWoutUnderline>
          </AppBlock>
        ))}
      </AppsContainer>
    );

    return (
      <div>
        <Header />
        {createNewApp}
        {existingApps}
      </div>
    );
  }
}

export default withAuth(MyAdminPage);
