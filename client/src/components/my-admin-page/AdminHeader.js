import React, { Fragment } from 'react';
import { Link as RouterLink } from 'react-router-dom';

import Icon from '@material-ui/core/Icon';

import SimpleMenu from '../../style/molecules/menu/';
import HeaderLink from '../../style/atomes/header/HeaderLink';
import Header from '../../style/molecules/header/';
import Logo from '../../style/atomes/logo';

const leftHeaderPart = (
  <Fragment>
    <HeaderLink component={RouterLink} to="/my-admin">
      <Logo>LogoD</Logo>
    </HeaderLink>
    <HeaderLink component={RouterLink} to="/my-admin">
      My Apps
    </HeaderLink>
    <HeaderLink component={RouterLink} to="/discover">
      Discover
    </HeaderLink>
    <HeaderLink component={RouterLink} to="/help">
      Help
    </HeaderLink>
  </Fragment>
);

const rightPartButtonContent = (
  <Fragment>
    <Icon className={'far fa-user-circle'} /> Account
  </Fragment>
);

const rightPart = (
  <SimpleMenu
    buttonContent={rightPartButtonContent}
    menuLinks={[
      { title: 'My account', url: '/my-admin/account' },
      { title: 'Logout', url: '/logout' },
    ]}
  />
);

const AdminHeader = () => (
  <Header leftPart={leftHeaderPart} rightPart={rightPart} />
);

export default AdminHeader;
