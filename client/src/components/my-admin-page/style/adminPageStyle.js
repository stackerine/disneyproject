import styled from 'styled-components';

import { PALE_WHITE } from '../../../style/constantes';

export const AppsContainer = styled.div`
  display: flex;
  justify-content: center;
  background-color: ${PALE_WHITE};
  padding: 20px;
  flex-wrap: wrap;
`;

export const AppBlock = styled.div`
  flex: 0 0 16%;
`;

export const AppTitle = styled.h3`
  margin: 0;
  margin-bottom: 10px;
  text-transform: uppercase;
`;

export const AppLogo = styled.div`
  width: 40%;
  padding-bottom: 40%; 
  background-image: url(${({ src }) => src});
  background-size:cover
  margin: auto;
`;

export const NewAppContainer = styled.div`
  border-bottom: black 3px solid;
`;

export const NewAppButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
`;
