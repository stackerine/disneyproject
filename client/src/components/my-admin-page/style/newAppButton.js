import React from 'react';
import { Link } from 'react-router-dom';

import styled from 'styled-components';

const LinkWoutUnderline = styled(Link)`
  text-decoration: none;
`;

const StyledButton = styled.div`
  overflow: hidden;
  position: relative;
  display: block;
  border: white 4px solid;
  color: white;
  padding: 15px 40px;
  background: transparent;
  text-transform: uppercase;
  font-size: 1.3rem;
  font-weight: 700;
  border-radius: 3px;
  cursor: pointer;
  :hover {
    color: #f06a36;
    transition: all 0.75s ease-in-out;
  }
  :after {
    content: '';
    position: absolute;
    height: 0%;
    left: 50%;
    top: 50%;
    width: 150%;
    z-index: -1;
    -webkit-transition: all 0.75s ease 0s;
    -moz-transition: all 0.75s ease 0s;
    -o-transition: all 0.75s ease 0s;
    transition: all 0.75s ease 0s;
    background: white;
    transform: translateX(-50%) translateY(-50%) rotate(25deg);
    transform-origin: center;
  }
  :hover:after {
    height: 450%;
    transform-origin: center;
  }
`;

export default ({ title, linkDir }) => {
  return (
    <LinkWoutUnderline to={linkDir}>
      <StyledButton>{title}</StyledButton>
    </LinkWoutUnderline>
  );
};
