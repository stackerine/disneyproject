import styled from 'styled-components';
import { PALE_GRAY } from '../../../style/constantes';

export const ModulesContainer = styled.div`
  border: ${PALE_GRAY} 1px solid;
  border-radius: 5px;
  margin-bottom: 20px;
  padding: 10px;
  background-color: white;
`;

export const ModuleTitle = styled.h4`
  margin-top: 0;
  margin-bottom: 10px;
  text-transform: capitalize;
`;

export const ModuleContent = styled.div`
  display: flex;
`;

export const ModuleDescription = styled.div`
  margin-left: 10px;
`;
