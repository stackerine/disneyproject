import styled from 'styled-components';

import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';

import { PALE_GRAY } from '../../../style/constantes';

export const ModuleContainer = styled.div`
  border: ${PALE_GRAY} 1px solid;
  border-radius: 2px;
  padding: 10px;
  margin-bottom: 5px;
  margin-top: 5px;
  background-color: white;
`;

export const ModuleHeader = styled.div`
  display: flex;
`;

export const ModuleTitle = styled.h3`
  margin-top: 0;
  margin-bottom: 10px;
`;

export const ButtonChangeTitle = styled(Button)`
  border: none;
  border-radius: 50%;
  height: 25px;
  width: 25px;
  min-width: 25px;
  margin-left: 10px;
  padding: 7px;
  &:focus {
    outline: none;
  }
`;

export const ModuleTitleEditing = styled(Input)`
  margin-bottom: 10px;
`;

export const OrderModuleButton = styled(Button)`
  float: right;
`;

export const AppModuleButtons = styled.div`
  margin: -5px;
  margin-top: 20px;
`;
