import styled from 'styled-components';

export const ModulesListContainer = styled.div`
  flex: 1;
  background-color: #bcbcbc;
  padding: 20px;
`;
