import styled from 'styled-components';

import Input from '@material-ui/core/Input';

export const PageApp = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

export const PageContainer = styled.div`
  display: flex;
  flex: 1;
`;

export const AppContainer = styled.div`
  flex: 3;
  padding: 20px;
`;

export const AppDetails = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const AppInfos = styled.div`
  flex: 1;
`;

export const InputWMarginBottom = styled(Input)`
  margin-bottom: 10px;
`;

export const AppBoardContainer = styled.div`
  border: #ccc 1px solid;
  border-radius: 5px;
  margin-top: 20px;
  padding: 15px 20px 15px 20px;
`;
