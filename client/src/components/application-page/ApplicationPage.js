import React, { Component } from 'react';
import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import { changeModuleTitleState } from './stateFunctions';

import AdminHeader from '../my-admin-page/AdminHeader';
import ModulesList from './ModulesList';
import AppBoard from './AppBoard';

import { saveApp, fetchApplication } from '../../services/applicationsCalls';
import { fetchModules } from '../../services/modulesCalls';

import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import {
  PageApp,
  PageContainer,
  AppContainer,
  AppDetails,
  AppInfos,
} from './style/application-page';

import InputWMarginBottom from '../../style/atomes/input/InputWMarginBottom';

import withAuth from '../../hoc/withAuth';

class ApplicationPage extends Component {
  state = {
    title: '',
    description: '',
    modules: [],
    loadModules: true,
    appModules: [],
    loadApp: true,
  };

  componentDidMount() {
    this.getModules();
    this.getApplication();
  }

  changeInput = (type, { target: { value } }) => {
    this.setState({ [type]: value });
  };

  save = async () => {
    const { title, description, appModules } = this.state;
    const {
      match: {
        params: { id },
      },
      history: { push },
    } = this.props;
    const { id: appId } = await saveApp({ id, title, description, appModules });

    if (!id) {
      push(`/application/${appId}`);
    } else {
      this.getApplication();
    }
  };

  getApplication = async () => {
    const {
      match: {
        params: { id },
      },
    } = this.props;
    if (id) {
      const { title, description, appModules } = await fetchApplication(id);

      this.setState({
        title,
        description,
        appModules: appModules.map(({ id, title, type, content }) => ({
          id,
          title,
          type,
          content,
        })),
        loadApp: false,
      });
    }
  };

  getModules = async () => {
    const modules = await fetchModules();
    this.setState({ modules, loadModules: false });
  };

  dropModule = ({ type }) => {
    this.setState(prevState => ({
      appModules: [...prevState.appModules, { type }],
    }));
  };

  orderModule = (oldIndex, newIndex) => {
    this.setState(prevState => {
      const { appModules } = prevState;
      const itemToMove = appModules[oldIndex];
      const appModulesWithoutItem = [
        ...appModules.slice(0, oldIndex),
        ...appModules.slice(oldIndex + 1),
      ];
      const newAppModules = [
        ...appModulesWithoutItem.slice(0, newIndex),
        itemToMove,
        ...appModulesWithoutItem.slice(newIndex),
      ];
      return { appModules: newAppModules };
    });
  };

  deleteModule = index => {
    this.setState(prevState => ({
      appModules: [
        ...prevState.appModules.slice(0, index),
        ...prevState.appModules.slice(index + 1),
      ],
    }));
  };

  editModule = () => {};

  changeModuleTitle = (index, { target: { value: title } }) =>
    this.setState(changeModuleTitleState(index, title));

  publish = () => {};

  render() {
    const { title, description, modules, appModules } = this.state;
    return (
      <DndProvider backend={HTML5Backend}>
        <PageApp>
          <AdminHeader />
          <PageContainer>
            <ModulesList modules={modules} />
            <AppContainer>
              <AppDetails>
                <AppInfos>
                  <InputWMarginBottom
                    type="text"
                    value={title}
                    placeholder="Title"
                    onChange={this.changeInput.bind(null, 'title')}
                  />
                  <InputWMarginBottom
                    fullWidth={true}
                    multiline={true}
                    type="text"
                    value={description}
                    placeholder="Description"
                    onChange={this.changeInput.bind(null, 'description')}
                  />
                </AppInfos>
                <div>
                  <ButtonGroup variant="contained">
                    <Button onClick={this.save}>Save</Button>
                    <Button onClick={this.publish}>Publish</Button>
                  </ButtonGroup>
                </div>
              </AppDetails>

              <AppBoard
                dropModule={this.dropModule}
                orderModule={this.orderModule}
                modules={appModules}
                deleteModule={this.deleteModule}
                editModule={this.editModule}
                changeModuleTitle={this.changeModuleTitle}
              />
            </AppContainer>
          </PageContainer>
        </PageApp>
      </DndProvider>
    );
  }
}

export default withAuth(ApplicationPage);
