import React, { Component } from 'react';
import { DragSource, DropTarget } from 'react-dnd';

import Text from './Text';
import Image from './Image';

import {
  ModuleContainer,
  OrderModuleButton,
  AppModuleButtons,
  ButtonChangeTitle,
  ModuleHeader,
  ModuleTitleEditing,
} from '../style/appModules';

import Icon from '@material-ui/core/Icon';

import ButtonWIcon from '../../../style/atomes/button/ButtonWIcon';
import { ModuleTitle } from '../style/appModules';

class AppModule extends Component {
  state = {
    isEditingTitle: false,
  };

  deleteThisModule = () => this.props.deleteModule(this.props.index);

  changeTitle = () =>
    this.setState(prevState => ({
      isEditingTitle: !prevState.isEditingTitle,
    }));

  render() {
    const {
      title = 'Module title',
      type,
      content,
      index,
      changeModuleTitle,
      connectDropTarget,
      connectDragPreview,
      connectDragSource,
    } = this.props;

    const { isEditingTitle } = this.state;

    const getModuleType = (type, content, index) => {
      const typeModule = {
        text: <Text content={content} index={index} />,
        image: <Image content={content} index={index} />,
      };
      return typeModule[type];
    };

    return connectDropTarget(
      <div>
        {connectDragPreview(
          <div>
            <ModuleContainer>
              {connectDragSource(
                <div>
                  <OrderModuleButton variant="outlined">
                    <Icon className="fas fa-sort" />
                  </OrderModuleButton>
                </div>
              )}

              <ModuleHeader>
                {(isEditingTitle && (
                  <ModuleTitleEditing
                    value={title}
                    onChange={changeModuleTitle.bind(null, index)}
                  />
                )) || <ModuleTitle>{title}</ModuleTitle>}
                <ButtonChangeTitle
                  onClick={this.changeTitle}
                  variant="contained"
                >
                  {(isEditingTitle && (
                    <Icon className="fas fa-save fa-sm" />
                  )) || <Icon className="fas fa-pen fa-sm" />}
                </ButtonChangeTitle>
              </ModuleHeader>

              {getModuleType(type, content, title, index)}
              <AppModuleButtons>
                <ButtonWIcon
                  variant="contained"
                  color="primary"
                  text="Edit"
                  icon="fas fa-edit fa-sm"
                />
                <ButtonWIcon
                  variant="contained"
                  color="secondary"
                  text="Delete"
                  icon="fas fa-trash fa-sm"
                  onClick={this.deleteThisModule}
                />
              </AppModuleButtons>
            </ModuleContainer>
          </div>
        )}
      </div>
    );
  }
}

const dragSpec = {
  beginDrag(props, monitor, component) {
    const item = { index: props.index };
    return item;
  },
};

function dragCollect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
  };
}

const dropSpec = {
  drop(props, monitor, component) {
    const item = monitor.getItem();
    const newIndex = props.index;
    const oldIndex = item.index;
    props.orderModule(oldIndex, newIndex);
    return item;
  },
};

function dropCollect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
  };
}

export default DropTarget('ITEM', dropSpec, dropCollect)(
  DragSource('ITEM', dragSpec, dragCollect)(AppModule)
);
