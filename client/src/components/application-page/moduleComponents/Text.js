import React from 'react';

const Text = ({ content = 'Text content' }) => {
  return (
    <div>
      <div>{content}</div>
    </div>
  );
};

export default Text;
