import React from 'react';

const Image = ({ content = 'Image content' }) => {
  return (
    <div>
      <div>{content}</div>
    </div>
  );
};

export default Image;
