import React from 'react';

import Module from './Module';
import Icon from '@material-ui/core/Icon';

import { ModulesListContainer } from './style/modules-list';

const ModulesList = ({ modules, loadModules }) => {
  const loader = loadModules && <Icon className="fas fa-spinner fa-spin" />;
  const modulesList =
    modules &&
    modules.map(({ id, name, description }) => (
      <Module key={id} name={name} id={id} description={description} />
    ));
  return <ModulesListContainer>{loader || modulesList}</ModulesListContainer>;
};

export default ModulesList;
