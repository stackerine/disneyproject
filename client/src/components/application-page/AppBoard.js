import React from 'react';
import { DropTarget } from 'react-dnd';

import { AppBoardContainer } from './style/application-page';

import AppModule from './moduleComponents/AppModule';

const AppBoard = ({
  modules,
  deleteModule,
  editModule,
  dropModule,
  orderModule,
  changeModuleTitle,
  isOver,
  canDrop,
  connectDropTarget,
}) => {
  const emptyModules = !modules.length && (
    <div>
      There is no module yet. You can add one from left menu, with drag and
      drop.
    </div>
  );

  const showModules = modules.map(({ type, title, content }, index) => (
    <AppModule
      key={index}
      type={type}
      title={title}
      content={content}
      index={index}
      orderModule={orderModule}
      deleteModule={deleteModule}
      changeModuleTitle={changeModuleTitle}
    />
  ));

  return connectDropTarget(
    <div>
      <AppBoardContainer>{emptyModules || showModules}</AppBoardContainer>
    </div>
  );
};

const spec = {
  drop(props, monitor, component) {
    const item = monitor.getItem();
    props.dropModule(item);
  },
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    canDrop: monitor.canDrop(),
  };
}

export default DropTarget('SOURCE', spec, collect)(AppBoard);
