import { changeModuleTitleState } from './stateFunctions';

describe('changeModuleTitleState', () => {
  test('should change title and return the correct object', () => {
    expect(
      changeModuleTitleState(1, 'NOUVEAU TITRE')({
        appModules: [{ id: 0 }, { id: 1, title: 'TITRE' }, { id: 2 }],
      })
    ).toEqual({
      appModules: [{ id: 0 }, { id: 1, title: 'NOUVEAU TITRE' }, { id: 2 }],
    });
  });

  test('should do nothing if there is no title or no id and title', () => {
    expect(
      changeModuleTitleState(1)({
        appModules: [{ id: 0 }, { id: 1, title: 'TITRE' }, { id: 2 }],
      })
    ).toEqual({
      appModules: [{ id: 0 }, { id: 1, title: 'TITRE' }, { id: 2 }],
    });
    expect(
      changeModuleTitleState()({
        appModules: [{ id: 0 }, { id: 1, title: 'TITRE' }, { id: 2 }],
      })
    ).toEqual({
      appModules: [{ id: 0 }, { id: 1, title: 'TITRE' }, { id: 2 }],
    });
  });
});
