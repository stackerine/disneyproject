import React from 'react';
import { DragSource } from 'react-dnd';

import Icon from '@material-ui/core/Icon';

import {
  ModulesContainer,
  ModuleTitle,
  ModuleContent,
  ModuleDescription,
} from './style/module';

const Module = ({ name, description, connectDragSource }) => {
  const iconClass = {
    image: 'fas fa-image',
    text: 'fas fa-align-left',
  };

  return connectDragSource(
    <div>
      <ModulesContainer>
        <ModuleTitle>{name}</ModuleTitle>
        <ModuleContent>
          <Icon className={iconClass[name]} />
          <ModuleDescription>{description}</ModuleDescription>
        </ModuleContent>
      </ModulesContainer>
    </div>
  );
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
  };
}

const spec = {
  beginDrag(props, monitor, component) {
    const item = { type: props.id };
    return item;
  },
};

export default DragSource('SOURCE', spec, collect)(Module);
