export const changeModuleTitleState = (index, title) => prevState => {
  if ((index || index === 0) && title) {
    const moduleToChange = prevState.appModules[index];
    return {
      appModules: [
        ...prevState.appModules.slice(0, index),
        { ...moduleToChange, title },
        ...prevState.appModules.slice(index + 1),
      ],
    };
  }
  return prevState;
};
