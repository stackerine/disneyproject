import React, { Component } from 'react';
import md5 from 'md5';
import queryString from 'query-string';

import MidPage from './MidPage';
import Header from '../my-admin-page/AdminHeader.js';

import withAuth from '../../hoc/withAuth';

import { createUser } from '../../services/users';
import { login, setUserToLocalStorage } from '../../services/auth';
import {
  isNotEmpty,
  isValidEmail,
  isValidPassword,
  isValidUsername,
} from '../../../../commons/formVerification';

import {
  ConnexionFullPage,
  ConnexionElements,
} from './style/connexionPageStyle';

const [SIGNUP, LOGIN] = ['SIGNUP', 'LOGIN'];

class ConnexionPage extends Component {
  state = {
    currentState: queryString.parse(this.props.location.search).login
      ? LOGIN
      : SIGNUP,
    emailRegister: '',
    usernameRegister: '',
    passwordRegister: '',
    emailLogin: '',
    passwordLogin: '',
    errors: {},
  };

  changeInput = (type, { target: { value } }) => {
    this.setState({ [type]: value });
  };

  changeCurrentState = () => {
    this.setState(prevState => ({
      currentState: prevState.currentState === SIGNUP ? LOGIN : SIGNUP,
    }));
  };

  signUp = async () => {
    const { emailRegister, usernameRegister, passwordRegister } = this.state;
    const { setAuthInformations } = this.props;
    if (this.checkSignUpForm()) {
      try {
        const { user, message, isSignedUp } = await createUser(
          emailRegister.toLowerCase(),
          usernameRegister,
          md5(passwordRegister)
        );
        setAuthInformations(user, message);
        this.setState({
          emailLogin: emailRegister,
          passwordLogin: passwordRegister,
        });
        isSignedUp && this.setState({ currentState: LOGIN });
      } catch (e) {
        setAuthInformations({}, 'An error occurred. ');
      }
    }
  };

  logIn = async () => {
    const { emailLogin, passwordLogin } = this.state;
    if (this.checkLogInForm()) {
      const { user, message } = await login(
        emailLogin.toLowerCase(),
        passwordLogin
      );
      const {
        checkAuth,
        setAuthInformations,
        history: { push },
      } = this.props;
      checkAuth();
      setUserToLocalStorage(user.id);
      setAuthInformations(user, message);
      this.props.isAuth && user && push('/my-admin');
    }
  };

  checkSignUpForm = () => {
    const { emailRegister, usernameRegister, passwordRegister } = this.state;

    const errEmail = !isValidEmail(emailRegister) && {
      invalidEmail: 'Please enter a valid email.',
    };
    const errUsername = !isValidUsername(usernameRegister) &&
      !isNotEmpty(usernameRegister) && {
        invalidUsername:
          'Please enter a valid password. It can contain letters & numbers.',
      };
    const errPassword = !isValidPassword(passwordRegister) && {
      invalidPassword:
        'Please enter a valid password (It can contain numbers & symbols, between 4 and 18 characters).',
    };

    const errForm = {
      ...errEmail,
      ...errUsername,
      ...errPassword,
    };

    this.setState({ errors: errForm });

    return !errEmail && !errUsername && !errPassword;
  };

  checkLogInForm = () => {
    const { emailLogin, passwordLogin } = this.state;

    const errEmail = !isValidEmail(emailLogin) && {
      invalidLogInEmail: 'Please enter your email.',
    };
    const errPassword = !isValidPassword(passwordLogin) && {
      invalidLogInPassword: 'Please enter your password',
    };

    const errForm = {
      ...errEmail,
      ...errPassword,
    };

    this.setState({ errors: errForm });

    return !errEmail && !errPassword;
  };

  render() {
    const {
      currentState,
      emailRegister,
      usernameRegister,
      passwordRegister,
      emailLogin,
      passwordLogin,
      errors,
    } = this.state;

    return (
      <ConnexionFullPage>
        <Header />
        <ConnexionElements>
          <MidPage
            action={this.signUp}
            type={SIGNUP}
            isOpen={currentState === SIGNUP}
            changeCurrentState={this.changeCurrentState}
            email={emailRegister}
            username={usernameRegister}
            password={passwordRegister}
            changeInput={this.changeInput}
            errors={errors}
          />
          <MidPage
            action={this.logIn}
            type={LOGIN}
            isOpen={currentState === LOGIN}
            changeCurrentState={this.changeCurrentState}
            email={emailLogin}
            password={passwordLogin}
            changeInput={this.changeInput}
            errors={errors}
          />
        </ConnexionElements>
      </ConnexionFullPage>
    );
  }
}

export default withAuth(ConnexionPage);
