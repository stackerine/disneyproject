import React, { Fragment } from 'react';

import {
  InputContainer,
  BlockTitle,
  ChangeStateLink,
  TextFieldMarged,
} from './style/connexionPageStyle';

const LogIn = ({
  email,
  password,
  changeInput,
  errors,
  changeCurrentState,
}) => {
  return (
    <Fragment>
      <InputContainer>
        <BlockTitle>Log in</BlockTitle>
        <TextFieldMarged
          type="email"
          value={email}
          onChange={changeInput.bind(null, 'emailLogin')}
          error={!!errors.invalidLogInEmail}
          label={errors.invalidLogInEmail || 'Email'}
        />
        <TextFieldMarged
          type="password"
          value={password}
          onChange={changeInput.bind(null, 'passwordLogin')}
          error={!!errors.invalidLogInEmail}
          label={errors.invalidLogInPassword || 'Password'}
        />
      </InputContainer>
      <span>
        You don't have an account?{' '}
        <ChangeStateLink onClick={changeCurrentState}>
          Create one !
        </ChangeStateLink>
      </span>
    </Fragment>
  );
};

export default LogIn;
