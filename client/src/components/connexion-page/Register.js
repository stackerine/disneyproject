import React, { Fragment } from 'react';

import {
  InputContainer,
  BlockTitle,
  TextFieldMarged,
  ChangeStateLink,
} from './style/connexionPageStyle';

const Register = ({
  email,
  username,
  password,
  changeInput,
  errors,
  changeCurrentState,
}) => {
  return (
    <Fragment>
      <InputContainer>
        <BlockTitle>Sign up</BlockTitle>
        <TextFieldMarged
          type="email"
          value={email}
          onChange={changeInput.bind(null, 'emailRegister')}
          error={!!errors.invalidEmail}
          label={errors.invalidEmail || 'Email'}
        />
        <TextFieldMarged
          type="text"
          value={username}
          onChange={changeInput.bind(null, 'usernameRegister')}
          error={!!errors.invalidUsername}
          label={errors.invalidUsername || 'Username'}
        />
        <TextFieldMarged
          type="password"
          value={password}
          onChange={changeInput.bind(null, 'passwordRegister')}
          error={!!errors.invalidPassword}
          label={errors.invalidPassword || 'Password'}
        />
      </InputContainer>
      <span>
        You already have an account?{' '}
        <ChangeStateLink onClick={changeCurrentState}>Log in !</ChangeStateLink>
      </span>
    </Fragment>
  );
};

export default Register;
