import React, { Component } from 'react';
import { logout } from '../../services/auth';

export default class LogoutPage extends Component {
  componentDidMount() {
    logout();
    this.props.history.push('/');
  }
  render() {
    return <div />;
  }
}
