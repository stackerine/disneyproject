import React, { Component, Fragment } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import {
  ConnexionFullPage,
  FullGradientPage,
} from './style/connexionPageStyle';
import { CenteredText } from './style/emailConfirmationStyle';
import Button from '../../style/atomes/button/comicStyleButton.js';

import { checkEmailConfirmationToken } from '../../services/auth';

export default class EmailConfirmation extends Component {
  state = {
    isLoading: true,
    status: '',
    message: '',
  };
  componentDidMount() {
    this.checkUserEmail();
  }

  checkUserEmail = async () => {
    const {
      match: {
        params: { token = '' },
      },
    } = this.props;
    this.setState({
      isLoading: true,
    });

    const { status, message } = await checkEmailConfirmationToken(token);

    this.setState({
      isLoading: false,
      status,
      message,
    });
  };

  render() {
    const { status, message, loading } = this.state;

    const loadingMsg = loading && (
      <Fragment>
        <h2>Please wait during confirmation</h2>
        <CircularProgress />;
      </Fragment>
    );
    const success = status === 'success' && (
      <Fragment>
        <h4>Hey Baaaaper</h4>
        <h2>Your email is now confirmed</h2>
        <p>You can fully enjoy Baaap application</p>
        <Button>Let's Go !</Button>
      </Fragment>
    );

    const error = status === 'error' && (
      <Fragment>
        <h4>Ouch, an error occured</h4>
        <h2>{message}</h2>
      </Fragment>
    );

    const content = loadingMsg || error || success;

    return (
      <ConnexionFullPage>
        <FullGradientPage>
          <CenteredText>{content}</CenteredText>
        </FullGradientPage>
      </ConnexionFullPage>
    );
  }
}
