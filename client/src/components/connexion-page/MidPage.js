import React from 'react';
import { Transition } from 'react-transition-group';

import Register from './Register';
import Login from './LogIn';

import {
  ConnexionMid,
  AnimatedBlock,
  transitionStyles,
} from './style/connexionPageStyle';

import Button from '../../style/atomes/button/comicStyleButton';

const MidPage = ({
  action,
  type,
  isOpen,
  changeCurrentState,
  ...otherProps
}) => {
  return (
    <ConnexionMid isOpen={isOpen}>
      <Transition
        timeout={500}
        in={isOpen}
        mountOnEnter
        unmountOnExit
        classNames="animBlock"
      >
        {state => (
          <AnimatedBlock style={{ ...transitionStyles[state] }}>
            {type === 'LOGIN' ? (
              <Login {...otherProps} changeCurrentState={changeCurrentState} />
            ) : (
              <Register
                {...otherProps}
                changeCurrentState={changeCurrentState}
              />
            )}
          </AnimatedBlock>
        )}
      </Transition>
      <Button
        variant="contained"
        onClick={(isOpen && action) || changeCurrentState}
      >
        {type === 'LOGIN' ? 'Log in' : 'Sign up'}
      </Button>
    </ConnexionMid>
  );
};

export default MidPage;
