import styled from 'styled-components';

export const CenteredText = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: white;
  text-align: center;
  padding: 50px;
  margin-bottom: 20px;
  line-height: 0;
`;
