import styled from 'styled-components';

import TextField from '@material-ui/core/TextField';

export const ConnexionFullPage = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const ConnexionElements = styled.div`
  display: flex;
  justify-content: space-between;
  height: 100%;
`;

const yellowGradient = `linear-gradient(
  135deg,
  rgba(255, 192, 75, 1) 0%,
  rgba(255, 165, 0, 1) 100%
)`;

export const FullGradientPage = styled.div`
  height: 100%;
  width: 100%;
  min-width: 200px;
  background: ${() => yellowGradient};
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;

export const ConnexionMid = styled(FullGradientPage)`
  width: ${({ isOpen }) => (isOpen && '85%') || '15%'};
  min-width: 200px;
  background: ${({ isOpen }) => (isOpen && yellowGradient) || 'white'};
  transition: all 0.7s cubic-bezier(0.75, -0.5, 0.3, 1.2);
  :first-of-type {
    border-right: black 3px solid;
  }
`;

export const BlockTitle = styled.h2`
  margin-top: 0;
  text-transform: uppercase;
`;

export const InputContainer = styled.div`
  display: flex;
  flex-direction: column;
  background-color: white;
  padding: 25px;
  border-radius: 5px;
  margin-bottom: 20px;
`;

export const TextFieldMarged = styled(TextField)`
  margin-bottom: 15px;
`;

export const AnimatedBlock = styled.div`
  transition: all 500ms ease;
  min-width: 600px;
  margin-bottom: 30px;
`;

export const transitionStyles = {
  entering: { opacity: 0, maxHeight: '0px', transform: 'translateY(0px)' },
  entered: { opacity: 1, maxHeight: '500px', transform: 'translateY(0px)' },
  exiting: { opacity: 0, maxHeight: '0px', transform: 'translateY(-300px)' },
  exited: { opacity: 0, maxHeight: '0px', transform: 'translateY(-300px)' },
};

export const ChangeStateLink = styled.span`
  color: blue;
  font-weight: 700;
  cursor: pointer;
`;
