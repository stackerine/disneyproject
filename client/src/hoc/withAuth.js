import React, { Component } from 'react';
import FullScreenContainer from '../style/atomes/container/fullScreenContainer';
import { FeedbackContainer, Feedback } from './style/withAuthStyle';
import { getAuthToken, getUserToLocalStorage } from '../services/auth';

const withAuth = WrappedComponent => {
  return class extends Component {
    state = {
      isAuth: false,
      user: {},
      message: '',
    };

    componentDidMount = () => {
      this.checkAuth();
    };

    checkAuth = () => {
      const hasToken = !!getAuthToken();
      hasToken
        ? this.setState({ isAuth: true, user: { id: getUserToLocalStorage() } })
        : this.setState({ isAuth: false }, this.props.history.push('/login'));
    };

    setAuthInformations = (user, message) => {
      this.setState({ user, message });
    };

    render() {
      const { message, isAuth, user } = this.state;
      const feedback = (
        <FeedbackContainer>
          <Feedback>
            <span>{message}</span>
          </Feedback>
        </FeedbackContainer>
      );
      return (
        <FullScreenContainer>
          {message && feedback}
          <WrappedComponent
            {...this.props}
            setAuthInformations={this.setAuthInformations}
            checkAuth={this.checkAuth}
            isAuth={isAuth}
            user={user}
          />
        </FullScreenContainer>
      );
    }
  };
};

export default withAuth;
