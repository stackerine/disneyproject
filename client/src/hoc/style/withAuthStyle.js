import styled from 'styled-components';
import { PALE_BLACK, PALE_WHITE } from '../../style/constantes';

export const FeedbackContainer = styled.div`
  width: 100%;
  position: absolute;
  height: 100px;
`;

export const Feedback = styled.div`
  width: 500px;
  position: relative;
  margin: 0 auto;
  background-color: ${PALE_WHITE};
  padding: 10px;
  color: ${PALE_BLACK};
  top: 20px;
  text-align: center;
  box-shadow: 0px 0px 5px 1px ${PALE_WHITE};
`;
