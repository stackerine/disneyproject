import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import LandingPage from './components/landingPage/Home';
import Login from './components/connexion-page/ConnexionPage';
import Logout from './components/connexion-page/LogoutPage';
import MyAdminPage from './components/my-admin-page/MyAdminPage';
import Application from './components/application-page/ApplicationPage';
import { StylesProvider } from '@material-ui/styles';
import AccountPage from './components/account/AccountPage';
import EmailConfirmationPage from './components/connexion-page/EmailConfirmationPage';

function App() {
  return (
    <StylesProvider injectFirst>
      <div className="App">
        <BrowserRouter>
          <Route exact path="/" component={LandingPage} />
          <Route path="/login" component={Login} />
          <Route path="/logout" component={Logout} />
          <Route
            path="/email-confirmation/:token?"
            component={EmailConfirmationPage}
          />
          <Route exact path="/my-admin" component={MyAdminPage} />
          <Route path="/my-admin/account" component={AccountPage} />
          <Route path="/application/:id" component={Application} />
        </BrowserRouter>
      </div>
    </StylesProvider>
  );
}

export default App;
