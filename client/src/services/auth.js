import axios from 'axios';

const TOKEN_ID = 'APP_TOKEN';
const USER_ID = 'USER_ID';

export const login = async (email, password) => {
  try {
    const {
      data: { user, token, message },
    } = await axios.post('/api/v1/auth', { email, password });
    localStorage.setItem(TOKEN_ID, token);
    return { user, message };
  } catch {
    return { message: 'Incorrect authentification.' };
  }
};

export const logout = () => {
  localStorage.removeItem(TOKEN_ID);
  localStorage.removeItem(USER_ID)
};

export const getAuthToken = () => localStorage.getItem(TOKEN_ID);

export const setUserToLocalStorage = userId =>
  localStorage.setItem(USER_ID, userId);

export const getUserToLocalStorage = () => localStorage.getItem(USER_ID);
export const checkEmailConfirmationToken = async token => {
  try {
    const {
      data: { status, message },
    } = await axios.get(`/api/v1/email-confirmation/${token}`);
    return { status, message };
  } catch {
    return {
      status: 'error',
      message: 'An error occurred. Please try again later...',
    };
  }
};
