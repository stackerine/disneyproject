import axios from 'axios';
import { BASE_URL } from './constantes';
import { getAuthToken } from './auth';

export const fetchModules = async () => {
  try {
    const { data } = await axios.get(`${BASE_URL}/modules`, {
      headers: { Authorization: `Bearer ${getAuthToken()}` },
    });
    return data;
  } catch (e) {
    console.log(e);
  }
};
