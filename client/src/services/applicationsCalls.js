import axios from 'axios';
import { BASE_URL } from './constantes';
import { getAuthToken } from './auth';

export const saveApp = async ({ id, title, description, appModules }) => {
  try {
    const { data } = await axios.post(
      `${BASE_URL}/applications`,
      { id, title, description, appModules },
      { headers: { Authorization: `Bearer ${getAuthToken()}` } }
    );
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const fetchUserApplication = async userId => {
  try {
    const { data } = await axios.get(
      `${BASE_URL}/users/${userId}/applications`,
      { headers: { Authorization: `Bearer ${getAuthToken()}` } }
    );
    return data;
  } catch (e) {
    console.log(e);
  }
};

export const fetchApplication = async appId => {
  try {
    const { data } = await axios.get(`${BASE_URL}/applications/${appId}`, {
      headers: { Authorization: `Bearer ${getAuthToken()}` },
    });
    return data;
  } catch (e) {
    console.log(e);
  }
};
