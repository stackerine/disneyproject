import axios from 'axios';
import { getAuthToken } from './auth';

export const createUser = async (email, username, password) => {
  const { data, status } = await axios.post('/api/v1/users', {
    email,
    username,
    password,
  });
  return { data, status };
};

export const getUser = async () => {
  const { data: userInfo } = await axios.get('/api/v1/users/me', {
    headers: { Authorization: `Bearer ${getAuthToken()}` },
  });
  return userInfo;
};

export const updateUserPassword = async (currentPassword, newPassword) => {
  const { data: serverFeedback } = await axios.put(
    '/api/v1/user/pswd',
    { currentPassword, newPassword },
    { headers: { Authorization: `Bearer ${getAuthToken()}` } }
  );
  return serverFeedback;
};

export const updateUserEmail = async (currentEmail, newEmail) => {
  const { data: serverFeedback } = await axios.put(
    '/api/v1/user/email',
    { currentEmail, newEmail },
    { headers: { Authorization: `Bearer ${getAuthToken()}` } }
  );
  return serverFeedback;
};

export const updateUserInfo = async ({ newUsername, newPhoneNumber }) => {
  const { data: userInfo } = await axios.put(
    '/api/v1/user',
    { newUsername, newPhoneNumber },
    { headers: { Authorization: `Bearer ${getAuthToken()}` } }
  );
  return userInfo;
};

export const removeUserInfo = async fieldName => {
  const { data: userInfo } = await axios.patch(
    `/api/v1/user`,
    { fieldName },
    {
      headers: { Authorization: `Bearer ${getAuthToken()}` },
    }
  );
  return userInfo;
};
