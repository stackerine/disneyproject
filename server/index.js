const express = require('express');
const config = require('./config');
const routes = require('./routes');
const middlewares = require('./middlewares/index');

const app = express();

middlewares(app);
routes(app);

app.listen(config.PORT, () => {
  console.log(`Server started on port ${config.PORT}`);
});
