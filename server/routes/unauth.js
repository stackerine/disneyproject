const express = require('express');
const UserCtrl = require('../controllers/user');
const AuthCtrl = require('../controllers/auth');
const unauth = express.Router();

unauth.post('/auth', AuthCtrl.authentification);
unauth.post('/users', UserCtrl.createUser);
unauth.get('/users/applications', UserCtrl.listUsersApplications);
unauth.get('/email-confirmation/:token', UserCtrl.validateEmailToken);
unauth.post('/email-confirmation', UserCtrl.sendUserValidationEmail);

module.exports = unauth;
