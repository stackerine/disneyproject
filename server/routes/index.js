const unauthRoutes = require('./unauth');
const authRoutes = require('./auth');
const passport = require('passport');

module.exports = function(app) {
  app.use('/api/v1', unauthRoutes);
  app.use(
    '/api/v1',
    passport.authenticate('jwt', { session: false }),
    authRoutes
  );
};
