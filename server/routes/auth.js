const express = require('express');
const auth = express.Router();

const {
  addApplication,
  getApplication,
} = require('../controllers/applications');
const {
  listUsersApplications,
  getUser,
  putUserPassword,
  putUserInfo,
  putUserEmail,
  removeUserInfo,
  putUserUsername,
} = require('../controllers/user');
const { listModules } = require('../controllers/modules');

auth.post('/applications', addApplication);
auth.get('/applications/:id', getApplication);
auth.get('/modules', listModules);
auth.get('/users/me', getUser);
auth.get('/users/:id/applications', listUsersApplications);
auth.put('/user', putUserInfo);
auth.put('/user/pswd', putUserPassword);
auth.put('/user/email', putUserEmail);
auth.patch('/user', removeUserInfo);

module.exports = auth;
