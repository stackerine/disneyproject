const bodyParser = require('body-parser');
const myStrategies = require('./auth');
const statics = require('./statics');

module.exports = function(app) {
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(bodyParser.json());
  statics(app);
  myStrategies(app);
};
