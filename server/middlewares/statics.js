const express = require('express');

module.exports = function(app) {
  const PATH_TO_STATIC = `${__dirname}/../../client/public/assets/`;
  app.use('/assets/', express.static(PATH_TO_STATIC));
};
