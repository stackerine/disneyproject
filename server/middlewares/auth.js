const LocalStrategy = require('passport-local').Strategy;
const findUserByEmail = require('../controllers/user').findUserByEmail;
const passport = require('passport');
const md5 = require('md5');
const passportJWT = require('passport-jwt');
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

module.exports = function(app) {
  passport.use(
    'local',
    new LocalStrategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      async function(email, password, cb) {
        const user = await findUserByEmail(email);
        if (!user) {
          return cb(null, false, {
            message: 'Incorrect authentification.',
          });
        }
        if (user.password !== md5(password)) {
          return cb(null, false, {
            message: 'Incorrect authentification.',
          });
        }
        if (!user.isVerified) {
          return cb(null, false, {
            message: 'Your account was not activated',
          });
        }
        return cb(null, user, {
          message: 'Logged In Successfully!',
        });
      }
    )
  );
  passport.use(
    'jwt',
    new JWTStrategy(
      {
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.JWT,
        expiresIn: '12h',
      },
      async function({ user: { email } }, cb) {
        return await findUserByEmail(email)
          .then(user => {
            const { id, email } = user;
            return cb(null, {
              id,
              email,
            });
          })
          .catch(err => {
            return cb(err);
          });
      }
    )
  );
  app.use(passport.initialize());
};
