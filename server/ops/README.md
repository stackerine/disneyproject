# Disney project Ops

## Update local environment variables

Environment variables are encrypted in ansible vault `env_vars`. To install ansible, use `sudo apt-get install ansible`.

You can: 

 - view the file: `ansible-vault view env_vars`
 - edit the file: `ansible-vault edit env_vars`

To update your local environment variables with vault ones, you can use the script `exportVaultToEnv.sh`.

⚠️ Before to use it, please add execution rights `chmod u+x exportVaultToEnv.sh`.

To exports vault variables in your environment, please use: `source exportVaultToEnv.sh`