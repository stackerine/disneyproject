#/bin/sh
DIRECTORY=$(cd `dirname $0` && pwd)
ansible-vault view $DIRECTORY/env_vars | while read line; do export $line ; done