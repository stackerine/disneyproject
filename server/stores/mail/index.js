const mailgun = require('mailgun-js');
const emailConfirmationTemplate = require('./templates/emailConfirmation.js');

const FROM_MAIL = 'BAAAP Admin <admin@baaap.com>';
const DOMAIN = process.env.MAIL_DOMAIN;
const mg = mailgun({
  apiKey: process.env.MAIL_API_KEY,
  domain: DOMAIN,
});

const sendHtmlEmail = ({ to, subject, html }) => {
  const data = {
    from: FROM_MAIL,
    to,
    subject,
    html,
  };
  mg.messages().send(data, function(error, body) {
    console.log(body);
  });
};

const sendEmailConfirmation = ({ to, token }) => {
  if (process.env.NODE_ENV === 'production') {
    const values = {
      mail_conf_url: `${
        process.env.REACT_APP_HOST
      }/email-confirmation/${token}`,
    };
    const result = emailConfirmationTemplate(values);
    const data = {
      subject: 'BAAAP: Email confirmation',
      from: FROM_MAIL,
      to,
      html: result,
    };
    mg.messages().send(data, function(error, body) {
      console.log(body);
    });
  }
};

module.exports = {
  sendHtmlEmail,
  sendEmailConfirmation,
};
