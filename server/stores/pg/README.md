## Generate a new model
npx sequelize-cli model:create --name Module --attributes name:string,description:string

## Generate seed
npx sequelize-cli seed:generate --name nameOfYourSeed
