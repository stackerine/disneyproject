const crypto = require('crypto-random-string');

const VerificationToken = require('./models').VerificationToken;
const User = require('./models').User;

const findTokenByToken = async token => {
  const verificationToken = await VerificationToken.findOne({
    include: [
      {
        model: User,
        as: 'user',
      },
    ],
    where: {
      token: token,
    },
  });
  return verificationToken ? verificationToken.dataValues : null;
};

const findTokenByEmail = async email => {
  const verificationToken = await VerificationToken.findOne({
    include: [
      {
        model: User,
        as: 'user',
        where: {
          email,
        },
      },
    ],
  });
  return verificationToken ? verificationToken.dataValues : null;
};

const renewToken = token => {
  const cryptoToken = crypto({ length: 64, type: 'url-safe' });

  return VerificationToken.update(
    {
      token: cryptoToken,
      updatedAt: Date.now(),
    },
    { where: { id: token.id } }
  );
};

module.exports = {
  findTokenByToken,
  findTokenByEmail,
  renewToken,
};
