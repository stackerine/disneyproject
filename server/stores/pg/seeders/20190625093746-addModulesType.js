'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'ModulesTypes',
      [
        { name: 'text', description: 'A simple text module' },
        { name: 'image', description: 'A simple image module' },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('ModulesTypes', null, {});
  },
};
