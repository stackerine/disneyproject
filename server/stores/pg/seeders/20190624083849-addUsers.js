const md5 = require('md5');

('use strict');

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Users',
      [
        {
          email: 'toto@gmail.com',
          password: md5('azerty'),
          username: 'toto',
          phone_number: '',
          isVerified: true,
        },
        {
          email: 'tutu@gmail.com',
          password: md5('azerty'),
          username: 'tutu',
          phone_number: '',
          isVerified: true,
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('User', null, {});
  },
};
