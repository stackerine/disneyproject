'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Modules',
      [
        {
          type: '1',
          content: '{ "test": true }',
          title: 'Mon module 1',
          application_id: 1,
        },
        {
          type: '2',
          content: '{ "test": true }',
          title: 'Mon module 2',
          application_id: 1,
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Modules', null, {});
  },
};
