'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'Applications',
      [
        {
          title: 'First App',
          description: 'First App description',
          order: [1, 2],
        },
        { title: 'Second App', description: 'Second App Description' },
        { title: 'Third App', description: 'Second App Description' },
        { title: 'Fourth App', description: 'Second App Description' },
        { title: 'Fifth App', description: 'Second App Description' },
        { title: 'Sixth App', description: 'Second App Description' },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Applications', null, {});
  },
};
