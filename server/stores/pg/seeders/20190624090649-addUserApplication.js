'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      'UserApplications',
      [
        { user_id: 1, application_id: 1, is_admin: true },
        { user_id: 1, application_id: 2, is_admin: false },
        { user_id: 2, application_id: 2, is_admin: false },
        { user_id: 1, application_id: 3, is_admin: false },
        { user_id: 1, application_id: 4, is_admin: false },
        { user_id: 1, application_id: 5, is_admin: false },
        { user_id: 1, application_id: 6, is_admin: false },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('UserApplications', null, {});
  },
};
