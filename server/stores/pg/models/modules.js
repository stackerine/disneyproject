'use strict';
module.exports = (sequelize, DataTypes) => {
  const Modules = sequelize.define(
    'Modules',
    {
      type: DataTypes.BIGINT,
      title: DataTypes.STRING,
      content: DataTypes.JSON,
      application_id: DataTypes.BIGINT,
    },
    {}
  );
  Modules.associate = function(models) {
    Modules.belongsTo(models.Application, {
      foreignKey: 'application_id',
    });
    Modules.belongsTo(models.ModulesTypes, {
      foreignKey: 'type',
      targetKey: 'id',
    });
  };
  return Modules;
};
