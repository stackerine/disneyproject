'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    'User',
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      username: DataTypes.STRING,
      phone_number: DataTypes.STRING,
      isVerified: DataTypes.BOOLEAN,
    },
    {}
  );
  User.associate = function(models) {
    User.belongsToMany(models.Application, {
      through: 'UserApplications',
      foreignKey: 'user_id',
      onDelete: 'CASCADE',
    });
  };
  return User;
};
