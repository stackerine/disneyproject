'use strict';
module.exports = (sequelize, DataTypes) => {
  const Application = sequelize.define(
    'Application',
    {
      title: DataTypes.STRING,
      description: DataTypes.STRING,
      order: DataTypes.ARRAY(DataTypes.BIGINT),
    },
    {}
  );
  Application.associate = function(models) {
    Application.hasMany(models.Modules, {
      as: 'appModules',
      foreignKey: 'application_id',
    });
    Application.belongsToMany(models.User, {
      through: 'UserApplications',
      foreignKey: 'application_id',
    });
  };
  return Application;
};
