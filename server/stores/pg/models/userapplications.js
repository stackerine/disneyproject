'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserApplications = sequelize.define(
    'UserApplications',
    {
      user_id: DataTypes.BIGINT,
      application_id: DataTypes.BIGINT,
      is_admin: DataTypes.BOOLEAN,
    },
    {
      timestamps: false,
    }
  );
  UserApplications.associate = function(models) {
    UserApplications.belongsTo(models.Application, {
      foreignKey: 'application_id',
      targetKey: 'id',
    });
    UserApplications.belongsTo(models.User, {
      foreignKey: 'user_id',
      targetKey: 'id',
    });
  };
  return UserApplications;
};
