'use strict';
module.exports = (sequelize, DataTypes) => {
  const ModulesTypes = sequelize.define(
    'ModulesTypes',
    {
      name: DataTypes.STRING,
      description: DataTypes.STRING,
    },
    {}
  );
  ModulesTypes.associate = function(models) {
    // associations can be defined here
  };
  return ModulesTypes;
};
