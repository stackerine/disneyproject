'use strict';
module.exports = (sequelize, DataTypes) => {
  const VerificationToken = sequelize.define(
    'VerificationToken',
    {
      user_id: DataTypes.BIGINT,
      token: DataTypes.STRING,
    },
    {}
  );
  VerificationToken.associate = function(models) {
    VerificationToken.belongsTo(models.User, {
      as: 'user',
      foreignKey: 'user_id',
      foreignKeyConstraint: true,
    });
  };
  return VerificationToken;
};
