module.exports = {
  development: {
    username: process.env.DISNEY_PG_DEV_USERNAME,
    password: process.env.DISNEY_PG_DEV_PASSWORD,
    database: 'disney_db',
    host: process.env.DISNEY_PG_DEV_HOST,
    dialect: 'postgres',
    logging: Boolean(process.env.PG_LOGGING),
  },
  test: {
    username: process.env.DISNEY_PG_TEST_USERNAME,
    password: process.env.DISNEY_PG_TEST_PASSWORD,
    database: 'disney_db',
    host: process.env.DISNEY_PG_TEST_HOST,
    dialect: 'postgres',
    logging: false,
  },
  production: {
    username: process.env.DISNEY_PG_PROD_USERNAME,
    password: process.env.DISNEY_PG_PROD_PASSWORD,
    database: 'disney_db',
    host: process.env.DISNEY_PG_PROD_HOST,
    dialect: 'postgres',
    logging: false,
  },
};
