const admin = require('./');

const createCustomTokenForUser = uid => {
  try {
    return admin.auth().createCustomToken(uid);
  } catch (e) {
    console.log('Error creating custom token:', error);
  }
};

module.exports = {
  createCustomTokenForUser,
};
