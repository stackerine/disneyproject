// Documentation: https://googleapis.dev/nodejs/firestore/latest/CollectionReference.html
const admin = require('./');

const db = admin.firestore();

const getDataByAppId = async appId => {
  try {
    const collection = db.collection(appId);
    const snapshot = await collection.get();

    const data = snapshot.docs.reduce(
      (docs, doc) => ({ [doc.id]: doc.data() }),
      {}
    );
    console.log(data);
  } catch (e) {
    console.error('Error getting documents', e);
  }
};
// getDataByAppId('apps');

const createApp = async () => {
  try {
    let app1Ref = db.collection('apps').doc('app1');
    let transaction = db
      .runTransaction(t => {
        return t.get(app1Ref).then(doc => {
          t.create(app1Ref, {
            name: 'app1',
            description: 'First App',
          });
          const modulesCollection = app1Ref
            .collection('modules')
            .doc('module1');

          t.create(modulesCollection, {
            name: 'module 1',
            description: 'Module 1 description',
          });
        });
      })
      .then(result => {
        console.log('Transaction success', result);
      })
      .catch(err => {
        console.log('Transaction failure:', err);
      });
  } catch (e) {
    console.error('Error getting documents', e);
  }
};

// createApp();

const getModuleInfo = async (appName, moduleName) => {
  try {
    const ref = db.doc(`apps/${appName}/modules/${moduleName}`);

    const snapshot = await ref.get();

    console.log(snapshot.data());
  } catch (e) {
    console.error('Error getting documents', e);
  }
};

// getModuleInfo('app1', 'module1');

const subscribeToUpdate = async appName => {
  try {
    const doc = db.collection('apps').doc('app1');

    const unsub = doc.onSnapshot(
      docSnapshot => {
        console.log(`Received doc snapshot: ${docSnapshot}`);

        console.log(docSnapshot.data());
      },
      err => {
        console.log(`Encountered error: ${err}`);
      }
    );

    setTimeout(() => {
      const app1Ref = db.collection('apps').doc('app1');
      app1Ref.set({
        name: `app1-${Date.now()}`,
      });
    }, 3000);
  } catch (e) {
    console.error('Error subscribing to document', e);
  }
};

const subscribeToApps = () => {
  db.collection('apps')
    .where('subscribers', 'array-contains', '1')
    .onSnapshot(function(querySnapshot) {
      console.log(querySnapshot.docs);
      // let apps = [];
      // querySnapshot.forEach(function(doc) {
      //   cities.push(doc.data().name);
      // });
      // console.log('Current cities in CA: ', cities.join(', '));
    });
};

subscribeToApps();

// subscribeToUpdate('app1');
