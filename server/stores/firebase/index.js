const admin = require('firebase-admin');
const serviceAccount = require('./firebase-adminsdk.js');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: `https://${process.env.DISNEY_FIREBASE_DEV_NAME}.firebaseio.com`,
});

module.exports = admin;
