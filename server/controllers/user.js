const crypto = require('crypto-random-string');
const moment = require('moment');
const User = require('../stores/pg/models').User;
const sequelize = require('../stores/pg/models').sequelize;
const VerificationToken = require('../stores/pg/models').VerificationToken;
const Application = require('../stores/pg/models').Application;
const UserApplications = require('../stores/pg/models').UserApplications;
const { sendEmailConfirmation } = require('../stores/mail/');
const VerificationTokensUtils = require('../stores/pg/verificationTokens');

const {
  isValidUsername,
  isValidEmail,
  isValidPhoneNumber,
  isNotEmpty,
} = require('../../commons/formVerification');

const listUsers = async (req, res) => {
  try {
    const payload = await User.findAll({});

    res.send(payload);
  } catch (exception) {
    res.status(500).send(exception);
  }
};

const findUserByEmail = async email => {
  try {
    const data = await User.findOne({
      where: {
        email,
      },
    });
    return data ? data.dataValues : null;
  } catch (exception) {
    return exception;
  }
};

const getUser = async (req, res) => {
  try {
    const { email } = req.user;
    const { username, phone_number } = await findUserByEmail(email);
    res.send({ username, email, phoneNumber: phone_number }).status(200);
  } catch (exception) {
    return exception;
  }
};

const putUserPassword = async (req, res) => {
  try {
    const { currentPassword, newPassword } = req.body;
    const { email } = req.user;
    const {
      dataValues: { password },
    } = await findUserByEmail(email);
    if (currentPassword === newPassword) {
      return res.status(400).send({
        isError: true,
        message: 'Please enter a different password from the previous.',
      });
    }
    if (currentPassword !== password) {
      return res.status(200).send({
        isError: true,
        message: 'Your current password is wrong.',
      });
    }
    await User.update({ password: newPassword }, { where: { email } });
    return res.status(200).send({
      isError: false,
      message: 'Your password has been changed successfully!',
    });
  } catch (exception) {
    return exception;
  }
};

const putUserEmail = async (req, res) => {
  try {
    const { currentEmail, newEmail } = req.body;
    const { email } = req.user;
    if (email !== currentEmail) {
      return res.send({
        isError: true,
        message: 'Your current email is wrong.',
      });
    }
    if (isValidEmail(newEmail)) {
      await User.update(
        { email: newEmail },
        { where: { email: currentEmail } }
      );
      return res
        .send({
          isError: false,
          message: 'Your email has been changed successfully!',
        })
        .status(200);
    }
    return res
      .send({ isError: true, message: 'Your new email seems to be incorrect.' })
      .status(400);
  } catch (exception) {
    return res
      .send({
        isError: true,
        message: 'An error occurred, please try again later.',
      })
      .status(500);
  }
};

const putUserInfo = async (req, res) => {
  try {
    const { dataToUpdate, value } = req.body;
    const { email } = req.user;

    const isUsernameData =
      dataToUpdate === 'username' && isValidUsername(value);

    const isPhoneNumberData =
      dataToUpdate === 'phone_number' && isValidPhoneNumber(value);

    const updatedData =
      (isUsernameData || !isNotEmpty(value) || isPhoneNumberData) &&
      (await User.update(
        { [dataToUpdate]: value },
        {
          where: { email },
          returning: true,
        }
      ));

    return res
      .send({
        userInfo: updatedData[1][0].dataValues[dataToUpdate],
        isError: false,
        message: 'Success!',
      })
      .status(200);
  } catch (exception) {
    return res
      .send({
        message: 'An error occurred, please try again later.',
        isError: true,
      })
      .status(500);
  }
};

const removeUserInfo = async (req, res) => {
  try {
    const { fieldName } = req.body;
    const { email } = req.user;
    const updateResponse = await User.update(
      { [fieldName]: '' },
      {
        where: { email },
        returning: true,
      }
    );
    const { [fieldName]: response } = updateResponse[1][0].dataValues;

    return res
      .send({
        updatedInfo: response,
        message: `The ${fieldName} has been successfully removed.`,
      })
      .status(200);
  } catch (exception) {
    return res
      .send({ message: 'An error occurred, please try again later.' })
      .status(500);
  }
};

const createUser = async (req, res) => {
  try {
    const { username, password, email } = req.body;
    if (username && isValidUsername(username) && isValidEmail(email)) {
      const user = await findUserByEmail(email);
      if (!user) {
        const cryptoToken = crypto({ length: 64, type: 'url-safe' });
        await sequelize.transaction(async t => {
          const user = await User.create(
            {
              email: formattedEmail,
              password,
            },
            { transaction: t, returning: true }
          );
          await VerificationToken.create(
            {
              user_id: user.dataValues.id,
              token: cryptoToken,
            },
            { transaction: t }
          );
        });

        await sendEmailConfirmation({
          to: formattedEmail,
          token: cryptoToken,
        });

        return res
          .send({
            user: { username, email: formattedEmail },
            message: 'We just send you an email to activate your account.',
            isSignedUp: true,
          })
          .status(201);
      }
      return res.status(400).send({
        user: null,
        message: 'Email already taken.',
        isSignedUp: false,
      });
    }
    return res
      .send({ user: null, message: 'An error occurred', isSignedUp: false })
      .status(400);
  } catch (exception) {
    console.log(exception);
    res.status(500).send(exception);
  }
};

const listUsersApplications = async (req, res) => {
  try {
    const { id } = req.params;
    //TODO : CHECK user_id with user_id in server side

    const payload = await UserApplications.findAll({
      include: [
        {
          model: Application,
        },
      ],
      where: {
        user_id: id,
        is_admin: true,
      },
    });

    formatedResult = payload.map(
      ({ dataValues: { Application } }) => Application
    );
    res.send(formatedResult);
  } catch (e) {
    console.log(e);
    //TO DO: log errors in a DB
    res.sendStatus(500);
  }
};

const isValidToken = async ({ updatedAt } = { updatedAt: null }) => {
  if (!updatedAt) {
    return false;
  }

  const lastUpdate = moment(updatedAt);
  const diff = moment().diff(lastUpdate, 'minutes');

  return diff < 60;
};

/**
 * Check the confirmation token sended by the user.
 * If the token has expired, send a new one.
 * @param {*} req
 * @param {*} res
 */
const validateEmailToken = async (req, res) => {
  try {
    const {
      params: { token },
    } = req;

    const verifToken = await VerificationTokensUtils.findTokenByToken(token);

    if (!verifToken) {
      return res
        .send({ status: 'error', message: 'Bad activation link' })
        .status(400);
    }

    const {
      id,
      user_id: userId,
      user: { email },
    } = verifToken;

    if (!isValidToken(verifToken)) {
      await VerificationTokensUtils.renewToken(verifToken);

      await sendEmailConfirmation({
        to: email,
        token: cryptoToken,
      });

      return res
        .send({
          status: 'error',
          message:
            'Your activation link has expired. We just send you a new one.',
        })
        .status(400);
    }

    await sequelize.transaction(async t => {
      await User.update(
        {
          isVerified: true,
        },
        { transaction: t, where: { id: userId } }
      );
      await VerificationToken.destroy({ where: { id } }, { transaction: t });
    });

    return res.send({
      status: 'success',
      message: 'Your account was successfully activated. You can login now.',
    });
  } catch (e) {
    console.log(e);
    res.status(500);
  }
};

/**
 * Resend en email with token for account validation
 * @param {*} req
 * @param {*} res
 */
const sendUserValidationEmail = async (req, res) => {
  try {
    const { email } = req.body;

    const verifToken = await VerificationTokensUtils.findTokenByEmail(email);

    if (!verifToken) {
      return res.status(400).json({
        status: 'error',
        message: "This email doesn't exist.",
      });
    }

    await sendEmailConfirmation({
      to: email,
      token: verifToken.token,
    });

    return res.json({
      status: 'success',
      message: 'An email is on its way to get your validation.',
    });
  } catch (e) {
    return res.status(500).json('An error happened. Please try again later.');
  }
};

module.exports = {
  listUsers,
  listUsersApplications,
  createUser,
  findUserByEmail,
  getUser,
  putUserPassword,
  putUserInfo,
  putUserEmail,
  removeUserInfo,
  validateEmailToken,
  sendUserValidationEmail,
};
