const ModulesTypes = require('../stores/pg/models').ModulesTypes;

const listModules = async (req, res) => {
  try {
    const payload = await ModulesTypes.findAll({});
    res.send(payload);
  } catch (e) {
    console.log(e);
    //TO DO: log errors in a DB
    res.status(500);
  }
};

module.exports = { listModules };
