const passport = require('passport');
const jwt = require('jsonwebtoken');
const { createCustomTokenForUser } = require('../stores/firebase/auth');

const authentification = (req, res, next) => {
  passport.authenticate(
    'local',
    { session: false },
    (err, user, { message = '' }) => {
      if (err || !user) {
        return res.status(404).json({
          message: message || 'Something is not right',
          status: 'error',
        });
      }
      req.login(user, { session: false }, async err => {
        if (err) {
          res.send(err);
        }
        const token = jwt.sign({ user }, process.env.JWT);
        const firebaseToken = await createCustomTokenForUser(user.id);

        return res.json({
          user: { id: user.id, email: user.email },
          status: 'success',
          message: 'Connected !',
          token,
          firebaseToken,
        });
      });
    }
  )(req, res);
};

module.exports = { authentification };
