const Application = require('../stores/pg/models').Application;
const difference = require('lodash.difference');
const UserApplications = require('../stores/pg/models').UserApplications;
const Modules = require('../stores/pg/models').Modules;
const sequelize = require('../stores/pg/models').sequelize;

const addAppInFirebase = async (appId, modulesId) => {
  await db.runTransaction(async t => {
    const appToAdd = db.collection('apps').doc(appId);
    const doc = await t.get(appToAdd);

    if (!doc.exists) {
      t.create(appToAdd, {
        public: false,
        users: [],
        lastUpdate: Date.now(),
      });
    } else {
      t.update(appToAdd, {
        lastUpdate: Date.now(),
      });
    }

    await Promise.all(
      modulesId.map(moduleId => {
        const modulesCollection = appToAdd.collection('modules').doc(moduleId);

        t.create(modulesCollection, {});
      })
    );
  });
};

const removeModulesInFireBase = async (appId, modulesIdToDelete) => {
  await Promise.all(
    modulesIdToDelete.map(moduleId => {
      db.collection('apps')
        .doc(appId)
        .collection('modules')
        .doc(moduleId)
        .delete();
    })
  );
};

const addApplication = async (req, res) => {
  try {
    const { id = null, title, description, appModules } = req.body;

    sequelize.transaction(async t => {
      const app = await Application.upsert(
        { id, title, description, order: [] },
        { transaction: t, returning: true }
      );

      const {
        dataValues: { id: application_id },
      } = app[0];

      if (!id) {
        const userApp = await UserApplications.create({
          user_id: 1 /*TODO PUT USER_ID HERE !*/,
          application_id,
          is_admin: true,
        });
      }

      const modules = await Promise.all(
        appModules.map(({ id, title, type }) =>
          Modules.upsert(
            { id, title, type, application_id },
            { transaction: t, returning: true }
          )
        )
      );

      const newModulesIds = modules
        .filter(mod => mod[1])
        .map(mod => mod[0].dataValues.id);

      const modulesIds = modules.map(mod => mod[0].dataValues.id);

      const updateModules = await Application.update(
        { order: modulesIds },
        { transaction: t, where: { id: application_id } }
      );

      addAppInFirebase(application_id, newModulesIds);

      const allAppModules = await Modules.findAll({
        where: {
          application_id,
        },
      });

      const allAppModulesIds = allAppModules.map(({ id }) => id);

      const modulesToDelete = difference(allAppModulesIds, modulesIds);

      const deleteModules = await Modules.destroy({
        transaction: t,
        where: {
          id: modulesToDelete,
        },
      });

      removeModulesInFireBase(application_id, modulesToDelete);

      res.status(200).send({ id: application_id });
    });
  } catch (e) {
    console.log(e);
    res.sendStatus(500);
  }
};

getApplication = async (req, res) => {
  try {
    const { id } = req.params;
    const appDetails = await Application.findAll({
      where: {
        id,
      },
      include: [
        {
          as: 'appModules',
          model: Modules,
          where: {
            application_id: id,
          },
          required: false,
        },
      ],
    });

    const { order, appModules, ...otherInfos } = appDetails[0].dataValues;

    const resToSend = {
      ...otherInfos,
      appModules: order.length
        ? order
            .map(id =>
              appModules.find(
                ({ dataValues: { id: idToFind } }) => id === idToFind
              )
            )
            .map(({ dataValues }) => dataValues)
        : [],
    };

    res.status(200).send(resToSend);
  } catch (e) {
    console.log(e); //TODO LogError in a DB
    res.sendStatus(500);
  }
};

module.exports = { addApplication, getApplication };
